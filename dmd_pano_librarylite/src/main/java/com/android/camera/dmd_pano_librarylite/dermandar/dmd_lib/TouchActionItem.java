package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

class TouchActionItem {
    private TouchActionItem.TouchActionItemType actionType;
    private float x;
    private float y;

    public TouchActionItem(TouchActionItem.TouchActionItemType type, float x, float y) {
        this.actionType = type;
        this.x = x;
        this.y = y;
    }

    public TouchActionItem.TouchActionItemType getActionType() {
        return this.actionType;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public static enum TouchActionItemType {
        TouchStart,
        TouchMove,
        TouchEnd,
        TouchZoom,
        TouchPinchZoom;

        private TouchActionItemType() {
        }
    }
}
