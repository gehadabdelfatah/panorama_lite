package com.android.camera.dmd_pano_librarylite.dermandar.gallery;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.graphics.Bitmap;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

class PanoramaItem implements Comparable<PanoramaItem> {
    private SimpleDateFormat mSimpleDateFormat;
    private SimpleDateFormat mSimpleDateFormatLabel;
    private String mName;
    private String mPath;
    private Bitmap mBitmapThumbnail;

    public PanoramaItem(String name, String path) {
        this.mSimpleDateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
        this.mSimpleDateFormatLabel = new SimpleDateFormat("MMMM dd, yyyy");
        this.mName = name;
        this.mPath = path;
    }

    public PanoramaItem(String name, String path, Bitmap thumbnail) {
        this(name, path);
        this.mBitmapThumbnail = thumbnail;
    }

    public int compareTo(PanoramaItem another) {
        if(another == null) {
            return -1;
        } else {
            Date myDate = this.getCreationDate();
            Date anotherDate = another.getCreationDate();
            return myDate == null && anotherDate == null?0:(myDate != null && anotherDate == null?-1:(myDate == null && anotherDate != null?1:(myDate.after(anotherDate)?-1:1)));
        }
    }

    public Date getCreationDate() {
        try {
            if(this.mName.length() != 13) {
                throw new Exception();
            } else {
                return this.mSimpleDateFormat.parse(this.mName);
            }
        } catch (Exception var3) {
            File imageFile = new File(this.mPath);
            return new Date(imageFile.lastModified());
        }
    }

    public String getName() {
        return this.mName;
    }

    public String getPath() {
        return this.mPath;
    }

    public void setBitmapThumbnail(Bitmap thumbnail) {
        this.mBitmapThumbnail = thumbnail;
    }

    public Bitmap getBitmapThumbnail() {
        return this.mBitmapThumbnail;
    }

    public String generateLabel() {
        Date myDate = this.getCreationDate();
        return myDate == null?null:this.mSimpleDateFormatLabel.format(myDate);
    }
}
