package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

class ImageItem {
    private byte[] mData;
    private int mWidth;
    private int mHeight;
    private double mRoll;
    private double mPitch;
    private double mYaw1;
    private double mYaw2;

    public ImageItem(byte[] data, int width, int height, double roll, double pitch, double yaw1, double yaw2) {
        this.mData = data;
        this.mWidth = width;
        this.mHeight = height;
        this.mRoll = roll;
        this.mPitch = pitch;
        this.mYaw1 = yaw1;
        this.mYaw2 = yaw2;
    }

    public byte[] getData() {
        return this.mData;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public double getRoll() {
        return this.mRoll;
    }

    public double getPitch() {
        return this.mPitch;
    }

    public double getYaw1() {
        return this.mYaw1;
    }

    public double getYaw2() {
        return this.mYaw2;
    }
}
