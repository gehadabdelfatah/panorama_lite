package com.android.camera.dmd_pano_librarylite.dermandar.gallery;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

class PanoramaDataFile {
    private String TAG = "";
    public static final String ITEM_FX = "Fx";
    private File mFileData;
    private ArrayList<String> mListItems;
    private ArrayList<String> mListValues;

    public PanoramaDataFile(String path) {
        this.mFileData = new File(path);
        this.mListItems = new ArrayList();
        this.mListValues = new ArrayList();
        this.loadData();
    }

    public PanoramaDataFile(File file) {
        this.mFileData = file;
        this.mListItems = new ArrayList();
        this.mListValues = new ArrayList();
        this.loadData();
    }

    public boolean exists() {
        return this.mFileData.exists();
    }

    public String readValue(String item) {
        return !this.mListItems.contains(item)?null:(String)this.mListValues.get(this.mListItems.indexOf(item));
    }

    public void writeValue(String item, String value) {
        if(this.mListItems.contains(item)) {
            int index = this.mListItems.indexOf(item);
            this.mListValues.set(index, value);
        } else {
            this.mListItems.add(item);
            this.mListValues.add(value);
        }

    }

    public void saveData() {
        String allData = "";

        for(int i = 0; i < this.mListItems.size(); ++i) {
            allData = allData + (String)this.mListItems.get(i) + "\t" + (String)this.mListValues.get(i);
            if(i < this.mListItems.size() - 1) {
                allData = allData + "\n";
            }
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.mFileData, false);
            fileOutputStream.write(allData.getBytes());
            fileOutputStream.close();
        } catch (Exception var3) {
            Log.e(this.TAG, "Exception: " + var3.getMessage());
        }

    }

    private void loadData() {
        if(this.mFileData.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(this.mFileData);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

                String line;
                while((line = bufferedReader.readLine()) != null) {
                    String[] parts = line.split("\\t");
                    this.mListItems.add(parts[0]);
                    this.mListValues.add(parts[1]);
                }

                bufferedReader.close();
                fileInputStream.close();
            } catch (Exception var5) {
                Log.e(this.TAG, "Exception: " + var5.getMessage());
            }

        }
    }
}
