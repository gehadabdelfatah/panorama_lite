package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.List;

class ExposureManager {
    private Context mContext;
    private List<EffectItem> mListExposureItems;

    public ExposureManager(Context context) {
        this.mContext = context;
        this.mListExposureItems = new ArrayList();
        this.mListExposureItems.add(new EffectItem(0, "", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "exp_auto")), (Drawable)null, (Drawable)null, 0));
        this.mListExposureItems.add(new EffectItem(1, "", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "exp_locked")), (Drawable)null, (Drawable)null, 1));
        this.mListExposureItems.add(new EffectItem(2, "", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "exp_locked_start")), (Drawable)null, (Drawable)null, 2));
    }

    public List<EffectItem> getListExposureItems() {
        return this.mListExposureItems;
    }
}
