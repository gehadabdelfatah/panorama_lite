package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build.VERSION;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class CameraPreview extends SurfaceView implements Callback {
    private String TAG = "DMD_Library";
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private boolean mIsCameraStarted = false;
    private String mWhiteBalanceMode = null;
    private String mColorEffect = null;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.mCamera = camera;
        this.mHolder = this.getHolder();
        this.mHolder.addCallback(this);
        if(VERSION.SDK_INT < 11) {
            this.mHolder.setType(3);
        }

    }

    public void setCamera(Camera camera) {
        this.mCamera = camera;
    }

    public List<String> getListSupportedWhiteBalance() {
        return this.mCamera != null?this.mCamera.getParameters().getSupportedWhiteBalance():null;
    }

    public List<String> getListSupportedColorEffects() {
        if(this.mCamera != null) {
            if(Globals.isNexus4Device()) {
                ArrayList<String> list = new ArrayList();
                list.add("solarize");
                list.add("sepia");
                list.add("posterize");
                list.add("negative");
                list.add("mono");
                list.add("none");
                return list;
            } else {
                return this.mCamera.getParameters().getSupportedColorEffects();
            }
        } else {
            return null;
        }
    }

    public List<String> getListSupportedFlashModes() {
        return this.mCamera != null?this.mCamera.getParameters().getSupportedFlashModes():null;
    }

    public boolean isFlashModeAvailable(String mode) {
        if(this.mCamera.getParameters().getSupportedFlashModes() != null) {
            Iterator var2 = this.mCamera.getParameters().getSupportedFlashModes().iterator();

            while(var2.hasNext()) {
                String fmode = (String)var2.next();
                if(mode.equals(fmode)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isAutoExposureLockSupported() {
        return VERSION.SDK_INT >= 14 && this.mCamera != null?this.mCamera.getParameters().isAutoExposureLockSupported():false;
    }

    public void setAutoExposureLockStatus(boolean activated) {
        if(this.isAutoExposureLockSupported()) {
            Parameters parameters = this.mCamera.getParameters();
            parameters.setAutoExposureLock(activated);
            this.mCamera.setParameters(parameters);
        }

    }

    public void setFlashMode(String mode) {
        if(this.mCamera != null) {
            Parameters parameters = this.mCamera.getParameters();
            parameters.setFlashMode(mode);

            try {
                this.mCamera.setParameters(parameters);
            } catch (Exception var4) {
                ;
            }
        }

    }

    public boolean isAutoWhiteBalanceLockSupported() {
        return VERSION.SDK_INT >= 14 && this.mCamera != null?this.mCamera.getParameters().isAutoWhiteBalanceLockSupported():false;
    }

    public void setAutoWhiteBalanceLockStatus(boolean activated) {
        if(this.isAutoWhiteBalanceLockSupported()) {
            Parameters parameters = this.mCamera.getParameters();
            parameters.setAutoWhiteBalanceLock(activated);
            this.mCamera.setParameters(parameters);
        }

    }

    public String getSelectedWhiteBalance() {
        return this.mCamera != null?this.mCamera.getParameters().getWhiteBalance():"";
    }

    public void setWhiteBalance(String wb) {
        if(this.mCamera != null) {
            Parameters parameters = this.mCamera.getParameters();
            parameters.setWhiteBalance(wb);
            this.mWhiteBalanceMode = wb;

            try {
                this.mCamera.setParameters(parameters);
            } catch (Exception var4) {
                ;
            }
        }

    }

    public void setColorEffect(String ce) {
        if(this.mCamera != null) {
            Parameters parameters = this.mCamera.getParameters();
            parameters.setColorEffect(ce);
            this.mColorEffect = ce;

            try {
                this.mCamera.setParameters(parameters);
            } catch (Exception var4) {
                ;
            }
        }

    }

    public String getSelectedColorEffect() {
        return this.mColorEffect;
    }

    public float getCameraMegaPixelCount() {
        if(this.mCamera == null) {
            return 0.0F;
        } else {
            Size maxPicSize = this.getMaxSize(this.mCamera.getParameters().getSupportedPictureSizes());
            if(maxPicSize == null) {
                return 0.0F;
            } else {
                double megaPixelCount = (double)(maxPicSize.width * maxPicSize.height);
                megaPixelCount /= 1000000.0D;
                return (float)megaPixelCount;
            }
        }
    }

    public void setCameraParameters() {
        if(VERSION.SDK_INT < 14) {
            this.stopPreview();
        }

        Parameters parameters = this.mCamera.getParameters();
        if(parameters.getSupportedFocusModes() != null && parameters.getSupportedFocusModes().contains("auto")) {
            parameters.setFocusMode("auto");
        }

        if(parameters.getSupportedFlashModes() != null && parameters.getSupportedFlashModes().contains("off")) {
            parameters.setFlashMode("off");
        }

        if(this.mWhiteBalanceMode == null) {
            if(parameters.getSupportedWhiteBalance() != null && parameters.getSupportedWhiteBalance().contains("auto")) {
                parameters.setWhiteBalance("auto");
            }
        } else {
            parameters.setWhiteBalance(this.mWhiteBalanceMode);
        }

        if(this.mColorEffect == null) {
            if(parameters.getSupportedColorEffects() != null && parameters.getSupportedColorEffects().contains("none")) {
                parameters.setColorEffect("none");
            }
        } else {
            parameters.setColorEffect(this.mColorEffect);
        }

        Size maxPicSize = this.getSizeFirstBiggerThan(parameters.getSupportedPictureSizes(), 512, !Globals.isTablet(), 1.3333333333333333D);
        Size maxPreviewSize;
        if(maxPicSize != null) {
            parameters.setPictureSize(maxPicSize.width, maxPicSize.height);
        } else {
            maxPreviewSize = this.getSizeFirstLowerThan(parameters.getSupportedPictureSizes(), 1024, !Globals.isTablet(), 1.3333333333333333D);
            if(maxPreviewSize != null) {
                parameters.setPictureSize(maxPreviewSize.width, maxPreviewSize.height);
            } else {
                parameters.setPictureSize(640, 480);
            }
        }

        if(Globals.isNexus4Device()) {
            parameters.setPreviewSize(640, 480);
        } else if(Globals.IS_MAX_PREVIEW_SIZE) {
            maxPreviewSize = this.getBestSizeForRatio1(parameters.getSupportedPreviewSizes(), Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
            if(maxPreviewSize != null) {
                parameters.setPreviewSize(maxPreviewSize.width, maxPreviewSize.height);
            } else {
                maxPreviewSize = this.getBestSizeForRatio(parameters.getSupportedPreviewSizes(), Globals.SCREEN_ASPECT_RATIO);
                if(maxPreviewSize != null) {
                    parameters.setPreviewSize(maxPreviewSize.width, maxPreviewSize.height);
                } else {
                    parameters.setPreviewSize(640, 480);
                }
            }
        } else {
            parameters.setPreviewSize(640, 480);
        }

        if(parameters.getSupportedPreviewFormats() != null && parameters.getSupportedPreviewFormats().contains(Integer.valueOf(17))) {
            parameters.setPreviewFormat(17);
        }

        try {
            if(!Globals.isTablet()) {
                this.mCamera.setDisplayOrientation(90);
            }

            this.mCamera.setParameters(parameters);
        } catch (Exception var4) {
            ;
        }

    }

    public void startPreview() {
        if(this.mCamera != null && !this.mIsCameraStarted) {
            try {
                this.mCamera.startPreview();
                this.mIsCameraStarted = true;
            } catch (Exception var2) {
                Log.e(this.TAG, var2.getMessage());
            }
        }

    }

    public void stopPreview() {
        if(this.mCamera != null && this.mIsCameraStarted) {
            try {
                this.mCamera.stopPreview();
            } catch (Exception var5) {
                ;
            } finally {
                this.mIsCameraStarted = false;
            }
        }

    }

    public Size getMaxSize(List<Size> listSize) {
        if(listSize != null && listSize.size() != 0) {
            Size size = (Size)listSize.get(0);

            for(int i = 1; i < listSize.size(); ++i) {
                Size tmpSize = (Size)listSize.get(i);
                if(tmpSize.width > size.width || tmpSize.height > size.height) {
                    size = tmpSize;
                }
            }

            return size;
        } else {
            return null;
        }
    }

    public Size getBestSizeForRatio(List<Size> listSize, double ratio) {
        double minDeltaRatio = 1.7976931348623157E308D;
        Size bestSize = null;
        int maxDeltaPixel = (int)(0.25D * (double)Globals.SCREEN_WIDTH * (double)Globals.SCREEN_HEIGHT);
        Iterator var8 = listSize.iterator();

        while(var8.hasNext()) {
            Size size = (Size)var8.next();
            if(Math.abs(Globals.SCREEN_WIDTH * Globals.SCREEN_HEIGHT - size.width * size.height) < maxDeltaPixel) {
                double currentRatio = Globals.isTablet()?(double)size.height / (double)size.width:(double)size.width / (double)size.height;
                double deltaRatio = Math.abs(ratio - currentRatio);
                if(minDeltaRatio > deltaRatio) {
                    minDeltaRatio = deltaRatio;
                    bestSize = size;
                } else if(minDeltaRatio == deltaRatio && size.width * size.height > bestSize.width * bestSize.height) {
                    bestSize = size;
                }
            }
        }

        return bestSize;
    }

    public Size getBestSizeForRatio1(List<Size> listSize, int width, int height) {
        if(!Globals.isTablet()) {
            int tmp = width;
            width = height;
            height = tmp;
        }

        Size bestSize = null;
        double minDeltaRatio = 1.7976931348623157E308D;
        double ratio = (double)width / (double)height;
        Iterator var9 = listSize.iterator();

        while(true) {
            Size size;
            do {
                if(!var9.hasNext()) {
                    return bestSize;
                }

                size = (Size)var9.next();
            } while(size.width != width && size.height != height);

            double currentRatio = Globals.isTablet()?(double)size.height / (double)size.width:(double)size.width / (double)size.height;
            double deltaRatio = Math.abs(ratio - currentRatio);
            if(minDeltaRatio > deltaRatio) {
                minDeltaRatio = deltaRatio;
                bestSize = size;
            }
        }
    }

    public Size getSizeFirstBiggerThan(List<Size> listSize, int length, boolean checkWidth, double aspectRatio) {
        if(listSize != null && listSize.size() != 0 && length > 0) {
            Size optimalSize = null;

            for(int i = 0; i < listSize.size(); ++i) {
                Size size = (Size)listSize.get(i);
                int len = checkWidth?size.width:size.height;
                if(len >= length && (aspectRatio == -1.0D || aspectRatio == (double)size.width * 1.0D / ((double)size.height * 1.0D))) {
                    if(optimalSize == null) {
                        optimalSize = size;
                    } else if(size.width < optimalSize.width || size.height < optimalSize.height) {
                        optimalSize = size;
                    }
                }
            }

            return optimalSize;
        } else {
            return null;
        }
    }

    public Size getSizeFirstLowerThan(List<Size> listSize, int length, boolean checkWidth, double aspectRatio) {
        if(listSize != null && listSize.size() != 0 && length > 0) {
            Size optimalSize = null;

            for(int i = 0; i < listSize.size(); ++i) {
                Size size = (Size)listSize.get(i);
                int len = checkWidth?size.width:size.height;
                if(len <= length && (aspectRatio == -1.0D || aspectRatio == (double)size.width * 1.0D / ((double)size.height * 1.0D))) {
                    if(optimalSize == null) {
                        optimalSize = size;
                    } else if(size.width >= optimalSize.width || size.height >= optimalSize.height) {
                        optimalSize = size;
                    }
                }
            }

            return optimalSize;
        } else {
            return null;
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if(this.mCamera != null) {
                this.mCamera.setPreviewDisplay(holder);
            }
        } catch (IOException var3) {
            Log.d(this.TAG, "Error setting camera preview: " + var3.getMessage());
        }

    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if(this.mHolder.getSurface() != null && this.mCamera != null) {
            try {
                this.mCamera.setPreviewDisplay(this.mHolder);
                this.startPreview();
            } catch (Exception var6) {
                Log.d(this.TAG, "Error starting camera preview: " + var6.getMessage());
            }

        }
    }
}
