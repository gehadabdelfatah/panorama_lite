package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

interface CallbackInterfaceViewer {
    void onSingleTapConfirmed();

    void onFinishLoadingPanorama();

    void onFinishGeneratingEqui();
}
