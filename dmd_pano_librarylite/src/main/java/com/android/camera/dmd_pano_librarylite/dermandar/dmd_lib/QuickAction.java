package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

class QuickAction extends PopupWindows implements android.widget.PopupWindow.OnDismissListener {
    private View mRootView;
    private ImageView mArrowUp;
    private ImageView mArrowDown;
    private LayoutInflater mInflater;
    private ViewGroup mTrack;
    private ScrollView mScroller;
    private QuickAction.OnActionItemClickListener mItemClickListener;
    private QuickAction.OnDismissListener mDismissListener;
    private List<ActionItem> actionItems = new ArrayList();
    private List<RadioButton> radioItems = new ArrayList();
    private boolean mDidAction;
    private int mChildPos;
    private int mInsertPos;
    private int mAnimStyle;
    private int rootWidth = 0;
    private boolean mIsMarginEnabled = false;
    public static final int ANIM_GROW_FROM_LEFT = 1;
    public static final int ANIM_GROW_FROM_RIGHT = 2;
    public static final int ANIM_GROW_FROM_CENTER = 3;
    public static final int ANIM_REFLECT = 4;
    public static final int ANIM_AUTO = 5;

    public QuickAction(Context context) {
        super(context);
        this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
        this.setRootViewId(Globals.getResourseIdByName(this.mContext.getPackageName(), "layout", "popup_vertical"));
        this.mAnimStyle = 5;
        this.mChildPos = 0;
    }

    public void setMarginEnabledStatus(boolean enabled) {
        this.mIsMarginEnabled = enabled;
    }

    public ActionItem getActionItem(int index) {
        return (ActionItem)this.actionItems.get(index);
    }

    public List<ActionItem> getListActionItems() {
        return this.actionItems;
    }

    public List<RadioButton> getListRadioButtons() {
        return this.radioItems;
    }

    public void setRootViewId(int id) {
        this.mRootView = (ViewGroup)this.mInflater.inflate(id, (ViewGroup)null);
        this.mTrack = (ViewGroup)this.mRootView.findViewById(Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "tracks"));
        this.mArrowDown = (ImageView)this.mRootView.findViewById(Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_down"));
        this.mArrowUp = (ImageView)this.mRootView.findViewById(Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_up"));
        this.mScroller = (ScrollView)this.mRootView.findViewById(Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "scroller"));
        this.mRootView.setLayoutParams(new LayoutParams(-2, -2));
        this.setContentView(this.mRootView);
    }

    public void setAnimStyle(int mAnimStyle) {
        this.mAnimStyle = mAnimStyle;
    }

    public void setOnActionItemClickListener(QuickAction.OnActionItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public void addActionItem(ActionItem action) {
        this.actionItems.add(action);
        String title = action.getTitle();
        Drawable icon = action.getIcon();
        boolean selected = action.isSelected();
        LinearLayout container = new LinearLayout(this.mContext);
        container.setOrientation(LinearLayout.HORIZONTAL);
        android.widget.LinearLayout.LayoutParams containerLP = new android.widget.LinearLayout.LayoutParams(-2, -2);
        if(this.mIsMarginEnabled) {
            containerLP.topMargin = 3;
            containerLP.bottomMargin = 3;
        }

        container.setLayoutParams(containerLP);
        RadioButton radio = new RadioButton(this.mContext);
        android.widget.LinearLayout.LayoutParams radioLP = new android.widget.LinearLayout.LayoutParams(-2, -2);
        radioLP.gravity = 17;
        radio.setLayoutParams(radioLP);
        ImageView img = new ImageView(this.mContext);
        android.widget.LinearLayout.LayoutParams imgLP = new android.widget.LinearLayout.LayoutParams(-2, -2);
        imgLP.gravity = 17;
        img.setLayoutParams(imgLP);
        TextView text = new TextView(this.mContext);
        android.widget.LinearLayout.LayoutParams textLP = new android.widget.LinearLayout.LayoutParams(-2, -2);
        textLP.gravity = 17;
        if(this.mIsMarginEnabled) {
            textLP.leftMargin = 5;
        }

        text.setLayoutParams(textLP);
        this.radioItems.add(radio);
        if(icon != null) {
            img.setImageDrawable(icon);
        } else {
            img.setVisibility(8);
        }

        if(title != null) {
            text.setText(title);
        } else {
            text.setVisibility(8);
        }

        if(selected) {
            radio.setChecked(selected);
        }

        final int pos = this.mChildPos;
        final int actionId = action.getActionId();
        OnClickListener onClickListener = new OnClickListener() {
            public void onClick(View v) {
                if(QuickAction.this.mItemClickListener != null) {
                    QuickAction.this.mItemClickListener.onItemClick(QuickAction.this, pos, actionId);
                }

                if(!QuickAction.this.getActionItem(pos).isSticky()) {
                    QuickAction.this.mDidAction = true;
                    QuickAction.this.dismiss();
                }

            }
        };
        container.setOnClickListener(onClickListener);
        radio.setOnClickListener(onClickListener);
        img.setOnClickListener(onClickListener);
        text.setOnClickListener(onClickListener);
        container.setFocusable(true);
        container.setClickable(true);
        container.addView(radio);
        container.addView(img);
        container.addView(text);
        this.mTrack.addView(container, this.mInsertPos);
        ++this.mChildPos;
        ++this.mInsertPos;
    }

    public void show(View anchor) {
        this.preShow();
        this.mDidAction = false;
        int[] location = new int[2];
        anchor.getLocationOnScreen(location);
        Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] + anchor.getHeight());
        this.mRootView.measure(-2, -2);
        int rootHeight = this.mRootView.getMeasuredHeight();
        if(this.rootWidth == 0) {
            this.rootWidth = this.mRootView.getMeasuredWidth();
        }

        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        this.mWindowManager.getDefaultDisplay().getMetrics(mDisplayMetrics);
        int screenWidth = mDisplayMetrics.widthPixels;
        int screenHeight = mDisplayMetrics.heightPixels;
        int xPos;
        int arrowPos;
        if(anchorRect.left + this.rootWidth > screenWidth) {
            xPos = anchorRect.left - (this.rootWidth - anchor.getWidth());
            xPos = xPos < 0?0:xPos;
            arrowPos = anchorRect.centerX() - xPos;
        } else {
            if(anchor.getWidth() > this.rootWidth) {
                xPos = anchorRect.centerX() - this.rootWidth / 2;
            } else {
                xPos = anchorRect.left;
            }

            arrowPos = anchorRect.centerX() - xPos;
        }

        int dyTop = anchorRect.top;
        int dyBottom = screenHeight - anchorRect.bottom;
        boolean onTop = dyTop > dyBottom;
        int yPos;
        LayoutParams l;
        if(onTop) {
            if(rootHeight > dyTop) {
                yPos = 15;
                l = this.mScroller.getLayoutParams();
                l.height = dyTop - anchor.getHeight();
            } else {
                yPos = anchorRect.top - rootHeight;
            }
        } else {
            yPos = anchorRect.bottom;
            if(rootHeight > dyBottom) {
                l = this.mScroller.getLayoutParams();
                l.height = dyBottom;
            }
        }

        this.showArrow(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_down"):Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_up"), arrowPos);
        this.setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);
        this.mWindow.showAtLocation(anchor, 0, xPos, yPos);
        this.mScroller.post(new Runnable() {
            public void run() {
                QuickAction.this.mScroller.fullScroll(130);
            }
        });
    }

    private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
        int arrowPos = requestedX - this.mArrowUp.getMeasuredWidth() / 2;
        switch(this.mAnimStyle) {
            case 1:
                this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Left"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Left"));
                break;
            case 2:
                this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Right"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Right"));
                break;
            case 3:
                this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Center"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Center"));
                break;
            case 4:
                this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Reflect"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Reflect"));
                break;
            case 5:
                if(arrowPos <= screenWidth / 4) {
                    this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Left"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Left"));
                } else if(arrowPos > screenWidth / 4 && arrowPos < 3 * (screenWidth / 4)) {
                    this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Center"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Center"));
                } else {
                    this.mWindow.setAnimationStyle(onTop?Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopUpMenu_Right"):Globals.getResourseIdByName(this.mContext.getPackageName(), "style", "Animations_PopDownMenu_Right"));
                }
        }

    }

    private void showArrow(int whichArrow, int requestedX) {
        View showArrow = whichArrow == Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_up")?this.mArrowUp:this.mArrowDown;
        View hideArrow = whichArrow == Globals.getResourseIdByName(this.mContext.getPackageName(), "id", "arrow_up")?this.mArrowDown:this.mArrowUp;
        int arrowWidth = this.mArrowUp.getMeasuredWidth();
        showArrow.setVisibility(0);
        MarginLayoutParams param = (MarginLayoutParams)showArrow.getLayoutParams();
        param.leftMargin = requestedX - arrowWidth / 2;
        hideArrow.setVisibility(4);
    }

    public void setOnDismissListener(QuickAction.OnDismissListener listener) {
        this.setOnDismissListener(this);
        this.mDismissListener = listener;
    }

    public void onDismiss() {
        if(!this.mDidAction && this.mDismissListener != null) {
            this.mDismissListener.onDismiss();
        }

    }

    public interface OnDismissListener {
        void onDismiss();
    }

    public interface OnActionItemClickListener {
        void onItemClick(QuickAction var1, int var2, int var3);
    }
}
