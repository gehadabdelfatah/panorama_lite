package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.os.Build;

class FocalLengthReader {
    private FocalLengthReader.FLItem[] flItems = new FocalLengthReader.FLItem[]{new FocalLengthReader.FLItem("SAMSUNG", "GT-I9000", 34.5F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9100", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9100T", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "SC-02C", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "SHW-M250K", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "SHW-M250L", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "SHW-M250S", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "SPH-D710", 31.259F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9300", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9300T", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9305", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9305T", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9308", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E210K", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E210L", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E210S", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-T999", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-T999v", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-I747", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-I747m", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-N064", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SC-06D", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-R530", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-I535", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "SPH-L710", 28.495F), new FocalLengthReader.FLItem("SAMSUNG", "GT-S5830", 34.933F), new FocalLengthReader.FLItem("SAMSUNG", "GALAXY NEXUS", 34.545F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7000", 30.933F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7000B", 30.933F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9220", 30.933F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7003", 30.933F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7100", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7102", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N7108", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-i605", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-R950", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-i317", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-i317M", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-T889", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-T889V", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SPH-L900", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-N719", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-N025", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SC-02E", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E250K", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E250L", 28.784F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E250S", 28.784F), new FocalLengthReader.FLItem("HTC", "HTC DESIRE S", 34.53F), new FocalLengthReader.FLItem("SONY ERICSSON", "LT18I", 31.582F), new FocalLengthReader.FLItem("SONY ERICSSON", "LT18A", 31.582F), new FocalLengthReader.FLItem("HTC", "HTC ONE X", 27.957F), new FocalLengthReader.FLItem("SAMSUNG", "NEXUS S", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9020", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9020T", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9020A", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9023", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "SPH-D720", 34.207F), new FocalLengthReader.FLItem("SAMSUNG", "SHW-M200", 34.207F), new FocalLengthReader.FLItem("MOTOROLA", "XT910", 38.738F), new FocalLengthReader.FLItem("MOTOROLA", "XT912", 38.738F), new FocalLengthReader.FLItem("MOTOROLA", "DROID RAZR", 38.738F), new FocalLengthReader.FLItem("MOTOROLA", "XT889", 38.738F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P7500", 33.0F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P7510", 33.0F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P7511", 33.0F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P6200", 34.286F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P6210", 34.286F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P6800", 34.286F), new FocalLengthReader.FLItem("SAMSUNG", "GT-P6810", 34.286F), new FocalLengthReader.FLItem("SAMSUNG", "GT-N8000", 34.32F), new FocalLengthReader.FLItem("LGE", "NEXUS 4", 31.413F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9500", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9505", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9508", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "GT-I9502", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E300K", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E300L", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SHV-E300S", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-I337", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-M919", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-I545", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SPH-L720", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-R970", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SCH-I959", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SGH-N045", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SC-04E", 31.864F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900F", 37.87F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900H", 37.87F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900R4", 37.87F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900V", 37.87F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900RZWAUSC", 37.87F), new FocalLengthReader.FLItem("SAMSUNG", "SM-G900W8", 37.87F)};

    public FocalLengthReader() {
        if(Build.MANUFACTURER != null && Build.MODEL != null) {
            float fl = this.getFLOf(Build.MANUFACTURER, Build.MODEL);
            if(fl > 0.0F) {
                Globals.FOCAL_LENGTH = (double)fl;
            }
        }

    }

    private float getFLOf(String manufacturer, String model) {
        FocalLengthReader.FLItem[] var3 = this.flItems;
        int var4 = var3.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            FocalLengthReader.FLItem flItem = var3[var5];
            if(flItem.manufacturer.equals(manufacturer.toUpperCase()) && flItem.model.equals(model.toUpperCase())) {
                return flItem.value;
            }
        }

        return 0.0F;
    }

    class FLItem {
        String manufacturer;
        String model;
        float value;

        public FLItem(String manufacturer, String model, float value) {
            this.manufacturer = manufacturer;
            this.model = model;
            this.value = value;
        }
    }
}
