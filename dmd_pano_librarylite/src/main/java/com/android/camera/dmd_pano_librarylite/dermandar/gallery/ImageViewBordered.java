package com.android.camera.dmd_pano_librarylite.dermandar.gallery;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.widget.ImageView;

class ImageViewBordered extends ImageView {
    private Paint mPaint = new Paint();
    private int mBorderThickness = 24;
    private boolean mIsDrawBorder = true;

    public ImageViewBordered(Context context) {
        super(context);
        this.mPaint.setColor(-1);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setStrokeWidth((float)this.mBorderThickness);
    }

    public void setIsDrawBorder(boolean isDrawBorder) {
        this.mIsDrawBorder = isDrawBorder;
    }

    public void setBorderThickness(int borderThickness) {
        this.mBorderThickness = borderThickness;
        if(this.mPaint != null) {
            this.mPaint.setStrokeWidth((float)this.mBorderThickness);
        }

    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.mIsDrawBorder) {
            canvas.drawLine(0.0F, 0.0F, (float)this.getWidth(), 0.0F, this.mPaint);
            canvas.drawLine(0.0F, (float)this.getHeight(), (float)this.getWidth(), (float)this.getHeight(), this.mPaint);
            canvas.drawLine(0.0F, 0.0F, 0.0F, (float)this.getHeight(), this.mPaint);
            canvas.drawLine((float)this.getWidth(), 0.0F, (float)this.getWidth(), (float)this.getHeight(), this.mPaint);
        }

    }
}
