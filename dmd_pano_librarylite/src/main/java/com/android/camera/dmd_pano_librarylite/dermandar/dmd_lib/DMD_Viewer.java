package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.media.ExifInterface;
import android.opengl.GLES20;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import com.nativesystem.Core;

class DMD_Viewer {
    private DisplayMetrics mDisplayMetrics;
    private ViewerGLView mViewerGLView;
    private RelativeLayout mRelativeLayoutRoot;
    private Handler mHandler;
    private Timer mTimer;
    private long mTimerStep = 5L;
    private int mExtraDrawCount = 0;
    private CallbackInterfaceViewer mCallbackInterfaceViewer;
    private boolean mIsGenerateEqui;
    private boolean mIsInitEngine;
    private String mEquiPath;
    private int mEquiWidth;
    private int mEquiHeight;
    private int mEquiMaxWidth;
    private float mEquiCenter;
    private ScaleGestureDetector mScaleGestDetect;
    private GestureDetector mDoubleTapGestDetect;
    private float mScaleFactor = 1.0F;
    private int mActivePointerId;
    private int INVALID_POINTER_ID = -1;
    private TouchActionItem.TouchActionItemType lastActionItemType;
    private OnTouchListener mOnTouchListenerGLSurfaceView;
    private Runnable mRunnableFinishedGeneratingImage;
    private Runnable mRunnableFinishedLoadingPanorama;
    private GLSurfaceView2.Renderer mRenderer;

    DMD_Viewer() {
        this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchEnd;
        this.mOnTouchListenerGLSurfaceView = new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent ev) {
                if(DMD_Viewer.this.mScaleGestDetect.onTouchEvent(ev) && DMD_Viewer.this.mScaleGestDetect.isInProgress()) {
                    return true;
                } else {
                    final float xx;
                    if(DMD_Viewer.this.mDoubleTapGestDetect.onTouchEvent(ev)) {
                        final float x = ev.getX();
                        xx = ev.getY();
                        if(DMD_Viewer.this.lastActionItemType != TouchActionItem.TouchActionItemType.TouchStart && DMD_Viewer.this.lastActionItemType != TouchActionItem.TouchActionItemType.TouchMove) {
                            if(DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchEnd) {
                                DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchZoom;
                                DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                                    public void run() {
                                        Core.controlStart(x, xx);
                                        Core.controlZoom(x, xx);
                                    }
                                });
                            }
                        } else {
                            DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchZoom;
                            DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                                public void run() {
                                    Core.controlZoom(x, xx);
                                }
                            });
                        }

                        return true;
                    } else {
                        int action = ev.getAction();
                        final float xxx;
                        switch(action & 255) {
                            case 0:
                                xx = ev.getX();
                                xxx = ev.getY();
                                DMD_Viewer.this.mActivePointerId = ev.getPointerId(0);
                                if(DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchEnd) {
                                    DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchStart;
                                    DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                                        public void run() {
                                            Core.controlStart(xx, xxx);
                                        }
                                    });
                                }
                                break;
                            case 1:
                                DMD_Viewer.this.mScaleFactor = 1.0F;
                                DMD_Viewer.this.mActivePointerId = DMD_Viewer.this.INVALID_POINTER_ID;
                                if(DMD_Viewer.this.lastActionItemType != TouchActionItem.TouchActionItemType.TouchEnd) {
                                    DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchEnd;
                                    DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                                        public void run() {
                                            DMD_Viewer.this.mExtraDrawCount = 120;
                                            Core.controlEnd();
                                        }
                                    });
                                }
                                break;
                            case 2:
                                int pointerIndex = ev.findPointerIndex(DMD_Viewer.this.mActivePointerId);
                                if(pointerIndex < 0 || pointerIndex >= ev.getPointerCount()) {
                                    return false;
                                }

                                xxx = ev.getX(pointerIndex);
                                final float y = ev.getY(pointerIndex);
                                if(DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchStart || DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchMove) {
                                    DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchMove;
                                    DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                                        public void run() {
                                            DMD_Viewer.this.mExtraDrawCount = 120;
                                            Core.controlMove(xxx, y);
                                        }
                                    });
                                }
                                break;
                            case 3:
                                DMD_Viewer.this.mActivePointerId = DMD_Viewer.this.INVALID_POINTER_ID;
                        }

                        return true;
                    }
                }
            }
        };
        this.mRunnableFinishedGeneratingImage = new Runnable() {
            public void run() {
                try {
                    FileInputStream fis = new FileInputStream(DMD_Viewer.this.mEquiPath + ".data");
                    byte[] bWidth = new byte[4];
                    byte[] bHeight = new byte[4];
                    fis.read(bWidth, 0, 4);
                    fis.read(bHeight, 0, 4);
                    int width = Globals.byteArrayToInt(bWidth);
                    int height = Globals.byteArrayToInt(bHeight);
                    int imageLength = width * height * 4;
                    byte[] bData = new byte[imageLength];
                    fis.read(bData, 0, imageLength);
                    fis.close();
                    ByteBuffer byteBuffer = ByteBuffer.wrap(bData);
                    IntBuffer intBuffer = byteBuffer.asIntBuffer();
                    int[] intArray = new int[width * height];
                    intBuffer.get(intArray, 0, width * height);
                    Bitmap bitmap = Bitmap.createBitmap(intArray, height, width, Config.ARGB_8888);
                    if(bitmap != null) {
                        FileOutputStream fos = new FileOutputStream(DMD_Viewer.this.mEquiPath, false);
                        bitmap.compress(CompressFormat.JPEG, 95, fos);
                        fos.close();
                    }

                    ExifInterface exifInterface = new ExifInterface(DMD_Viewer.this.mEquiPath);
                    exifInterface.setAttribute("Orientation", "6");
                    exifInterface.saveAttributes();
                } catch (FileNotFoundException var13) {
                    var13.printStackTrace();
                } catch (IOException var14) {
                    var14.printStackTrace();
                }

                File fileEquiData = new File(DMD_Viewer.this.mEquiPath + ".data");
                fileEquiData.delete();
                Globals.IS_ALLOWED_TO_DRAW = true;
                Globals.mIsFirstDrawAfterStitch = true;
                DMD_Viewer.this.mExtraDrawCount = 120;
                DMD_Viewer.this.mViewerGLView.enableBufferSwap();
                if(DMD_Viewer.this.mCallbackInterfaceViewer != null) {
                    DMD_Viewer.this.mCallbackInterfaceViewer.onFinishGeneratingEqui();
                }

            }
        };
        this.mRunnableFinishedLoadingPanorama = new Runnable() {
            public void run() {
                Globals.IS_ALLOWED_TO_DRAW = true;
                Globals.mIsFirstDrawAfterStitch = true;
                DMD_Viewer.this.mExtraDrawCount = 120;
                DMD_Viewer.this.mViewerGLView.enableBufferSwap();
                if(DMD_Viewer.this.mCallbackInterfaceViewer != null) {
                    DMD_Viewer.this.mCallbackInterfaceViewer.onFinishLoadingPanorama();
                }

            }
        };
        this.mRenderer = new GLSurfaceView2.Renderer() {
            public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            }

            public void onSurfaceChanged(GL10 gl, int width, int height) {
            }

            public void onDrawFrame(GL10 gl) {
                if(DMD_Viewer.this.mIsGenerateEqui) {
                    DMD_Viewer.this.mIsGenerateEqui = false;
                    if(DMD_Viewer.this.mEquiCenter == -1.0F) {
                        Core.genEqui(DMD_Viewer.this.mEquiPath + ".data", DMD_Viewer.this.mEquiHeight, DMD_Viewer.this.mEquiWidth, DMD_Viewer.this.mEquiMaxWidth);
                    } else {
                        Core.genEqui1(DMD_Viewer.this.mEquiPath + ".data", DMD_Viewer.this.mEquiHeight, DMD_Viewer.this.mEquiWidth, DMD_Viewer.this.mEquiMaxWidth, DMD_Viewer.this.mEquiCenter);
                    }

                    DMD_Viewer.this.mHandler.post(DMD_Viewer.this.mRunnableFinishedGeneratingImage);
                } else if(Globals.mIsClear) {
                    GLES20.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
                    GLES20.glClear(16384);
                    Globals.mIsClear = false;
                } else {
                    if(Globals.mIsAutoPlay) {
                        Globals.mIsAutoPlay = false;
                        Core.controlAutoPlay();
                    }

                    if(Globals.IS_ALLOWED_TO_DRAW && (Core.controlAnimate() || DMD_Viewer.this.mExtraDrawCount > 0)) {
                        if(Globals.mIsFirstDrawAfterStitch) {
                            Core.controlSetDim(Globals.isTablet()?Globals.SCREEN_WIDTH:Globals.SCREEN_HEIGHT, Globals.isTablet()?Globals.SCREEN_HEIGHT:Globals.SCREEN_WIDTH);
                            Core.controlAutoPlay();
                            Globals.mIsFirstDrawAfterStitch = false;
                        }

                        DMD_Viewer.this.mExtraDrawCount--;
                        GLES20.glBindFramebuffer('赀', 0);
                        GLES20.glViewport(0, 0, Globals.isTablet()?Globals.SCREEN_WIDTH:Globals.SCREEN_HEIGHT, Globals.isTablet()?Globals.SCREEN_HEIGHT:Globals.SCREEN_WIDTH);
                        GLES20.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
                        GLES20.glClear(16384);
                        Core.controlDraw();
                    }

                }
            }
        };
    }

    public View initViewer(Context context, CallbackInterfaceViewer interfaceViewer, int displayRotation) {
        this.mDisplayMetrics = context.getResources().getDisplayMetrics();
        if(displayRotation != 0 && displayRotation != 2) {
            Globals.SCREEN_WIDTH = this.mDisplayMetrics.heightPixels;
            Globals.SCREEN_HEIGHT = this.mDisplayMetrics.widthPixels;
        } else {
            Globals.SCREEN_WIDTH = this.mDisplayMetrics.widthPixels;
            Globals.SCREEN_HEIGHT = this.mDisplayMetrics.heightPixels;
        }

        Globals.SCREEN_ASPECT_RATIO = (double)Globals.SCREEN_HEIGHT / (double)Globals.SCREEN_WIDTH;
        this.mCallbackInterfaceViewer = interfaceViewer;
        this.mScaleGestDetect = new ScaleGestureDetector(context, new DMD_Viewer.ScaleListener());
        this.mDoubleTapGestDetect = new GestureDetector(context, new DMD_Viewer.DoubleTapListener());
        this.mRelativeLayoutRoot = new RelativeLayout(context);
        this.mRelativeLayoutRoot.setLayoutParams(new LayoutParams(-1, -1));
        this.mViewerGLView = Globals.getMyGLSurfaceView();
        if(this.mViewerGLView == null) {
            this.mIsInitEngine = true;
            this.createViewer(context);
        }

        this.mHandler = new Handler();
        Globals.IS_ALLOWED_TO_DRAW = true;
        new FocalLengthReader();
        return this.mRelativeLayoutRoot;
    }

    public void genEquiAt(String equiPath, int equiHeight, int equiWidth, int equiMaxWidth) {
        this.mEquiPath = equiPath;
        this.mEquiHeight = Globals.equiHeight();
        this.mEquiWidth = equiWidth;
        this.mEquiMaxWidth = equiMaxWidth;
        this.mEquiCenter = -1.0F;
        Globals.IS_ALLOWED_TO_DRAW = false;
        this.mIsGenerateEqui = true;
    }

    public void genEquiAt(String equiPath, int equiHeight, int equiWidth, int equiMaxWidth, float center) {
        this.mEquiPath = equiPath;
        this.mEquiHeight = Globals.equiHeight();
        this.mEquiWidth = equiWidth;
        this.mEquiMaxWidth = equiMaxWidth;
        this.mEquiCenter = center;
        Globals.IS_ALLOWED_TO_DRAW = false;
        this.mIsGenerateEqui = true;
    }

    public int getEquiFx() {
        return Core.getFx();
    }

    private void createViewer(Context context) {
        this.mViewerGLView = new ViewerGLView(context, this.mRenderer);
        Globals.setMyGLSurfaceView(this.mViewerGLView);
        this.mViewerGLView.setOnTouchListener(this.mOnTouchListenerGLSurfaceView);
        this.mViewerGLView.setLayoutParams(new LayoutParams(-1, -1));
        this.mViewerGLView.enableBufferSwap();
        this.mViewerGLView.setRenderMode(0);
    }

    public void startViewer() {
        this.checkSurfaceView();
        this.mExtraDrawCount = 120;
        Globals.mIsFirstDrawAfterStitch = true;
        this.startTimer();
    }

    public void stopViewer() {
        this.stopTimer();
    }

    void startTimer() {
        if(this.mTimer == null) {
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    if(DMD_Viewer.this.mViewerGLView == null) {
                        DMD_Viewer.this.stopTimer();
                    } else {
                        DMD_Viewer.this.mViewerGLView.requestRender();
                    }

                }
            }, 0L, this.mTimerStep);
        }

    }

    void stopTimer() {
        if(this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }

    }

    private void checkSurfaceView() {
        if(this.mViewerGLView != null) {
            if(this.mViewerGLView.getParent() != null) {
                RelativeLayout mll = (RelativeLayout)this.mViewerGLView.getParent();
                mll.removeView(this.mViewerGLView);
                this.mViewerGLView.setRenderer(this.mRenderer);
                this.mViewerGLView.setOnTouchListener(this.mOnTouchListenerGLSurfaceView);
                this.mViewerGLView.setLayoutParams(new LayoutParams(-1, -1));
                this.mViewerGLView.setRenderMode(0);
                this.mViewerGLView.enableBufferSwap();
                this.mRelativeLayoutRoot.addView(this.mViewerGLView);
            } else {
                this.mViewerGLView.setRenderer(this.mRenderer);
                this.mViewerGLView.setOnTouchListener(this.mOnTouchListenerGLSurfaceView);
                this.mViewerGLView.setLayoutParams(new LayoutParams(-1, -1));
                this.mViewerGLView.setRenderMode(0);
                this.mViewerGLView.enableBufferSwap();
                this.mRelativeLayoutRoot.addView(this.mViewerGLView);
            }
        }

    }

    private String decimalToDMS(double coord) {
        double mod = coord % 1.0D;
        int intPart = (int)coord;
        String degrees = String.valueOf(intPart);
        coord = mod * 60.0D;
        mod = coord % 1.0D;
        intPart = (int)coord;
        if(intPart < 0) {
            intPart *= -1;
        }

        String minutes = String.valueOf(intPart);
        coord = mod * 60.0D;
        intPart = (int)coord;
        if(intPart < 0) {
            intPart *= -1;
        }

        String seconds = String.valueOf(intPart);
        String output = degrees + "/1," + minutes + "/1," + seconds + "/1";
        return output;
    }

    private class ScaleListener extends SimpleOnScaleGestureListener {
        private ScaleListener() {
        }

        public boolean onScale(ScaleGestureDetector detector) {
            DMD_Viewer.this.mScaleFactor = DMD_Viewer.this.mScaleFactor * detector.getScaleFactor();
            if(DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchStart || DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchPinchZoom || DMD_Viewer.this.lastActionItemType == TouchActionItem.TouchActionItemType.TouchMove) {
                DMD_Viewer.this.lastActionItemType = TouchActionItem.TouchActionItemType.TouchPinchZoom;
                final float scaleFactor = DMD_Viewer.this.mScaleFactor;
                DMD_Viewer.this.mViewerGLView.queueEvent(new Runnable() {
                    public void run() {
                        DMD_Viewer.this.mExtraDrawCount = 120;
                        Core.controlZoom1(scaleFactor);
                    }
                });
            }

            return true;
        }
    }

    private class DoubleTapListener implements OnGestureListener, OnDoubleTapListener {
        private DoubleTapListener() {
        }

        public boolean onDown(MotionEvent e) {
            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }

        public boolean onDoubleTapEvent(MotionEvent e) {
            return false;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            if(DMD_Viewer.this.mCallbackInterfaceViewer != null) {
                DMD_Viewer.this.mCallbackInterfaceViewer.onSingleTapConfirmed();
            }

            return false;
        }
    }
}
