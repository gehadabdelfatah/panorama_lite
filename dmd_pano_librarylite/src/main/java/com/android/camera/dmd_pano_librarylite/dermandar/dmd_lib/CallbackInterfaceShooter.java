package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.graphics.Bitmap;
import java.util.HashMap;

public interface CallbackInterfaceShooter {
    void preparingToShoot();

    void canceledPreparingToShoot();

    void takingPhoto();

    void shotTakenPreviewReady(Bitmap var1);

    void photoTaken();

    void stitchingCompleted(HashMap<String, Object> var1);

    void shootingCompleted(boolean var1);

    void onFinishGeneratingEqui();

    void deviceVerticalityChanged(int var1);

    void compassEvent(HashMap<String, Object> var1);

    void onGalleryIconClicked();

    void onCameraStarted();

    void onCameraStopped();

    void onFinishClear();

    void onFinishRelease();

    void onHDRIconClicked_InApp();

    void onDirectionUpdated(float var1);

    void onHDIconClicked_InApp();

    void onRotatorConnected();

    void onRotatorDisconnected();

    void onStartedRotating();

    void onFinishedRotating();
}
