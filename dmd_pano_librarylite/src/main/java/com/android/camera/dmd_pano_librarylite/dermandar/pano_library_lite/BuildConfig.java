package com.android.camera.dmd_pano_librarylite.dermandar.pano_library_lite;

/**
 * Created by Pc1 on 05/02/2018.
 */

public final class BuildConfig {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "com.dermandar.pano_library_lite";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 2;
    public static final String VERSION_NAME = "1.1";

    public BuildConfig() {
    }
}
