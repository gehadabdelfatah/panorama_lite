package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class FlashManager {
    private Context mContext;
    private List<EffectItem> mListFlashItems;

    public FlashManager(Context context) {
        this.mContext = context;
        this.mListFlashItems = new ArrayList();
        this.mListFlashItems.add(new EffectItem(0, "off", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "flash_off")), (Drawable)null, (Drawable)null, 0));
        this.mListFlashItems.add(new EffectItem(3, "torch", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "flash_torch")), (Drawable)null, (Drawable)null, 3));
    }

    public List<EffectItem> getListExposureItems() {
        return this.mListFlashItems;
    }

    public EffectItem getFlashModeItem(String key) {
        Iterator var2 = this.mListFlashItems.iterator();

        EffectItem item;
        do {
            if(!var2.hasNext()) {
                return null;
            }

            item = (EffectItem)var2.next();
        } while(!item.getKey().equals(key));

        return item;
    }
}
