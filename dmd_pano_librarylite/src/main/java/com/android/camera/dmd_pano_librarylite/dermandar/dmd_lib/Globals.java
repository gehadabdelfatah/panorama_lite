package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import java.io.FileFilter;
import java.util.Queue;
import java.util.regex.Pattern;

class Globals {
    private static ViewerGLView mMyGLSurfaceView;
    public static final String LOG_TAG = "DMD_Library";
    public static final String EXTRA_IMAGE_INDEX = "IMAGE_INDEX";
    public static final String EXTRA_IMAGES_LIST = "IMAGES_LIST";
    public static final String EXTRA_DEGREES_LIST = "DEGREES_LIST";
    public static final String LINK_FILE = "link.txt";
    public static final String OTHER_FILE = "other.txt";
    public static final String TAG_LONGITUDE = "longitude";
    public static final String TAG_LATITUDE = "latitude";
    public static final String TAG_ALTITUDE = "altitude";
    public static final String TAG_CTIME = "ctime";
    public static final String TAG_HEADING = "heading";
    public static final String TAG_LOCATION_NAME = "loc_name";
    public static final String TAG_LOCATION_DESCRIPTION = "loc_desc";
    public static final String TAG_IS_UPLOADED = "isUploaded";
    public static String THUMB_DIR;
    public static String DATA_DIR;
    public static final String PREFERENCE_NAME = "DMDPref";
    public static final String PREF_FIRST_RUN = "FirstRun";
    public static final String PREF_NUMBER_SHOT_PANORAMAS = "NbPanos";
    public static final String PREF_APP_RATED = "DMDRated";
    public static final String PREF_FOCAL_LENGTH = "DMDFL";
    public static final String PREF_PANO_NAME = "PANO_NAME";
    public static String MANUFACTURER_SAMSUNG = "SAMSUNG";
    public static String MANUFACTURER_LG = "LGE";
    public static String DEVICE_NEXUS4 = "NEXUS 4";
    public static final int CORE_DIRECTION_LEFT = 1;
    public static final int CORE_DIRECTION_RIGHT = -1;
    public static final double PREVIEW_MAX_VARIATION_PERCENTAGE = 0.25D;
    public static final int CAMERA_FIXED_PREVIEW_WIDTH = 640;
    public static final int CAMERA_FIXED_PREVIEW_HEIGHT = 480;
    public static final int CAMERA_NEXUS4_PREVIEW_WIDTH = 640;
    public static final int CAMERA_NEXUS4_PREVIEW_HEIGHT = 480;
    public static final int CAMERA_FIXED_IMAGE_WIDTH = 512;
    public static final int CAMERA_FIXED_IMAGE_HEIGHT = 512;
    public static final int CAMERA_FIXED_IMAGE_HD_WIDTH = 1024;
    public static final int CAMERA_FIXED_IMAGE_HD_HEIGHT = 1024;
    public static final int CAMERA_FIXED_IMAGE_HD2_WIDTH = 2048;
    public static final int CAMERA_FIXED_IMAGE_HD2_HEIGHT = 2048;
    public static int CAMERA_IMAGE_WIDTH;
    public static int CAMERA_IMAGE_HEIGHT;
    public static final double ASPECT_RATIO_4_3 = 1.3333333333333333D;
    public static final double YIN_YANG_RATIO = 0.26785714285714285D;
    public static final double YIN_YANG_RATIO_TABLET = 0.13392857142857142D;
    public static final double YIN_YANG_TOP_MARGIN_PERCENTAGE = 0.041666666666666664D;
    public static final double YIN_YANG_LEFT_RIGHT_MARGIN_PERCENTAGE = 0.0625D;
    public static final int EXPOSURE_NONE = -1;
    public static final int EXPOSURE_AUTO = 0;
    public static final int EXPOSURE_LOCKED = 1;
    public static final int EXPOSURE_LOCKED_START = 2;
    public static final String EFFECT_WASHED = "washed";
    public static final String EFFECT_VINTAGE_WARM = "vintage-warm";
    public static final String EFFECT_VINTAGE_COLD = "vintage-cold";
    public static final String EFFECT_POINT_BLUE = "point-blue";
    public static final String EFFECT_POINT_RED_YELLOW = "point-red-yellow";
    public static final String EFFECT_POINT_GREEN = "point-green";
    public static final String FLASH_OFF = "off";
    public static final String FLASH_ON = "on";
    public static final String FLASH_AUTO = "auto";
    public static final String FLASH_TORCH = "torch";
    public static final int MENU_ITEM_DISCARD_INDEX = 1;
    public static final int VIEW_CAMERA = 1;
    public static final int VIEW_GL = 2;
    public static final int RESET_NONE = 0;
    public static final int RESET_BACK_BUTTON = 1;
    public static final int RESET_ON_PAUSE = 2;
    public static final int RESET_CLICK = 3;
    public static final int RESET_RESTART = 4;
    public static final int SAVE_NONE = 0;
    public static final int SAVE_TAKE_ANOTHER = 1;
    public static final int SAVE_SHARE = 2;
    public static final int EXTRA_DRAW_STEPS_COUNT = 120;
    public static final int MIN_IMAGES_TO_STITCH = 2;
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static double SCREEN_ASPECT_RATIO;
    public static String LOCATION_NAME;
    public static String LOCATION_DESCRIPTION;
    public static double LOCATION_LONGITUDE;
    public static double LOCATION_LATITUDE;
    public static double LOCATION_ALTITUDE;
    public static float LOCATION_FIRST_SHOT_DIRECTION;
    public static long LOCATION_TIME;
    public static boolean LOCATION_IS_GPS;
    public static float TEXT_VIEW_INSTRUCTION_TEXT_SIZE = 32.0F;
    public static float BRIGHTNESS_AUTO = -1.0F;
    public static float BRIGHTNESS_FULL = 1.0F;
    public static final int VIEWER_MENU_CLOSE_DELAY = 5000;
    public static final int SAVE_ANIMATION_FADEOUT_DELAY = 400;
    public static final int PREVIEW_CAPTURE_ANIMATION_DURATION = 400;
    public static final int PICTURE_CAPTURE_ANIMATION_DURATION = 550;
    public static final int YIN_YANG_VISIBILITY_DELAY = 250;
    public static int MAX_LOADED_IMAGE = 30;
    public static final int MAX_TOLOAD_TO_ANIMATE = 20;
    public static final long SIZE_KB = 1024L;
    public static final long SIZE_MB = 1048576L;
    public static final long SIZE_KB_PIC = 100L;
    public static final long SIZE_KB_PIC_HD = 300L;
    public static double FOCAL_LENGTH = 32.0D;
    public static double EXPOSURE_TIME = 0.043D;
    public static final int FOVX_180 = 180;
    public static final int FOVX_360 = 360;
    public static int FOVX_MAX = 360;
    public static boolean IS_MAX_PREVIEW_SIZE = true;
    public static boolean IS_STITCH_WHILE_SHOOTING = false;
    public static boolean IS_ALLOWED_TO_DRAW = false;
    public static int NumberShotImages = 0;
    public static Queue<ImageItem> mListImageItems;
    public static boolean mIsTellEngineToStartStitching;
    public static boolean mIsStitchCurrentImage;
    public static boolean mIsReset;
    public static boolean mIsClear;
    public static boolean mIsStitch;
    public static boolean mIsGenEqui;
    public static boolean mIsCoreInitialized;
    public static boolean mIsInitMakViewer;
    public static boolean mIsFirstDrawAfterStitch;
    public static boolean mIsAutoPlay;
    public static String ORI_FOLDER_NAME;
    public static int mFovX;
    public static double ROLL;
    public static double PITCH;
    public static double PERCENTAGE;
    private static int mNumberCores = -1;

    Globals() {
    }

    public static ViewerGLView getMyGLSurfaceView() {
        return mMyGLSurfaceView;
    }

    public static void setMyGLSurfaceView(ViewerGLView myGLSurfaceView) {
        mMyGLSurfaceView = myGLSurfaceView;
    }

    public static int equiHeight() {
        return 800;
    }

    public static int getPanoHeight() {
        return 512;
    }

    public static boolean isTablet() {
        return SCREEN_ASPECT_RATIO < 1.0D;
    }

    public static int getNumCores() {
        if(mNumberCores > 0) {
            return mNumberCores;
        } else {
            try {
                File dir = new File("/sys/devices/system/cpu/");
                class CpuFilter implements FileFilter {
                    CpuFilter() {
                    }

                    public boolean accept(File pathname) {
                        return Pattern.matches("cpu[0-9]", pathname.getName());
                    }
                }

                File[] files = dir.listFiles(new CpuFilter());
                mNumberCores = files.length;
                return files.length;
            } catch (Exception var2) {
                mNumberCores = 1;
                return 1;
            }
        }
    }

    public static void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
        int frameSize = width * height;
        int j = 0;

        for(int yp = 0; j < height; ++j) {
            int uvp = frameSize + (j >> 1) * width;
            int u = 0;
            int v = 0;

            for(int i = 0; i < width; ++yp) {
                int y = (255 & yuv420sp[yp]) - 16;
                if(y < 0) {
                    y = 0;
                }

                if((i & 1) == 0) {
                    v = (255 & yuv420sp[uvp++]) - 128;
                    u = (255 & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = y1192 + 1634 * v;
                int g = y1192 - 833 * v - 400 * u;
                int b = y1192 + 2066 * u;
                if(r < 0) {
                    r = 0;
                } else if(r > 262143) {
                    r = 262143;
                }

                if(g < 0) {
                    g = 0;
                } else if(g > 262143) {
                    g = 262143;
                }

                if(b < 0) {
                    b = 0;
                } else if(b > 262143) {
                    b = 262143;
                }

                int index = (i + 1) * height - (j + 1);
                rgb[index] = -16777216 | r << 6 & 16711680 | g >> 2 & '\uff00' | b >> 10 & 255;
                ++i;
            }
        }

    }

    public static double getFreeSpaceInKB() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double sdAvailSize = (double)stat.getAvailableBlocks() * (double)stat.getBlockSize();
        return sdAvailSize / 1024.0D;
    }

    public static boolean isSamsungDevice() {
        try {
            return Build.MANUFACTURER.toUpperCase().equals(MANUFACTURER_SAMSUNG);
        } catch (Exception var1) {
            return false;
        }
    }

    public static boolean isNexus4Device() {
        try {
            return Build.MANUFACTURER.toUpperCase().equals(MANUFACTURER_LG) && Build.MODEL.toUpperCase().equals(DEVICE_NEXUS4);
        } catch (Exception var1) {
            return false;
        }
    }

    public static int getResourseIdByName(String packageName, String className, String name) {
        Class r = null;
        int id = 0;

        try {
            r = Class.forName(packageName + ".R");
            Class[] classes = r.getClasses();
            Class desireClass = null;

            for(int i = 0; i < classes.length; ++i) {
                if(classes[i].getName().split("\\$")[1].equals(className)) {
                    desireClass = classes[i];
                    break;
                }
            }

            if(desireClass != null) {
                id = desireClass.getField(name).getInt(desireClass);
            }
        } catch (ClassNotFoundException var8) {
            var8.printStackTrace();
        } catch (IllegalArgumentException var9) {
            var9.printStackTrace();
        } catch (SecurityException var10) {
            var10.printStackTrace();
        } catch (IllegalAccessException var11) {
            var11.printStackTrace();
        } catch (NoSuchFieldException var12) {
            var12.printStackTrace();
        }

        return id;
    }

    public static int byteArrayToInt(byte[] b) {
        return b[0] & 255 | (b[1] & 255) << 8 | (b[2] & 255) << 16 | (b[3] & 255) << 24;
    }
}
