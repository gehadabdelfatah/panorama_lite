package com.android.camera.dmd_pano_librarylite.dermandar.gallery;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;

@SuppressLint({"ViewConstructor"})
class PanoramaViewGroup extends RelativeLayout {
    private final int ID_PANO;
    private final int ID_NAME;
    private final int ID_NUMBER;
    private GalleryActivity mContext;
    private ImageViewBordered mImageViewPano;
    private TextView mTextViewName;
    private TextView mTextViewNumber;
    private int mImageNumber;
    private Bitmap mImageBitmap;
    private String mImageName;
    private String mImagePath;
    private String mImageLabel;
    private PanoramaViewGroup.OnClickListenerPano mOnClickListenerPano;
    private OnClickListener mOnClickListener;

    public PanoramaViewGroup(GalleryActivity context) {
        this(context, 0, "Empty", (String)null);
    }

    public PanoramaViewGroup(GalleryActivity context, int number, String name, String path) {
        super(context);
        this.ID_PANO = 1;
        this.ID_NAME = 2;
        this.ID_NUMBER = 4;
        this.mOnClickListener = new OnClickListener() {
            public void onClick(View v) {
                if(PanoramaViewGroup.this.mOnClickListenerPano != null) {
                    PanoramaViewGroup.this.mOnClickListenerPano.onClick(PanoramaViewGroup.this);
                }

            }
        };
        this.mContext = context;
        this.mImageNumber = number;
        this.mImageName = name;
        this.mImagePath = path;
        this.mImageBitmap = null;
        this.setOnClickListener(this.mOnClickListener);
        this.mImageViewPano = new ImageViewBordered(this.mContext);
        this.mImageViewPano.setIsDrawBorder(true);
        this.mImageViewPano.setBorderThickness(2);
        this.mImageViewPano.setId(1);
        this.mImageViewPano.setScaleType(ScaleType.FIT_CENTER);
        this.mContext.registerForContextMenu(this.mImageViewPano);
        LayoutParams lp1 = new LayoutParams(-1, -1);
        lp1.addRule(14);
        lp1.addRule(15);
        this.mImageViewPano.setLayoutParams(lp1);
        this.mImageViewPano.setOnClickListener(this.mOnClickListener);
        this.addView(this.mImageViewPano);
        this.mTextViewName = new TextView(this.mContext);
        this.mTextViewName.setId(2);
        this.mTextViewName.setText(name);
        this.mTextViewName.setTextColor(-1);
        this.mTextViewName.setTextSize(17.0F);
        this.mContext.registerForContextMenu(this.mTextViewName);
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.addRule(12);
        lp2.addRule(9);
        lp2.leftMargin = 5;
        lp2.bottomMargin = 5;
        this.mTextViewName.setLayoutParams(lp2);
        this.mTextViewName.setOnClickListener(this.mOnClickListener);
        this.addView(this.mTextViewName);
        this.mTextViewNumber = new TextView(this.mContext);
        this.mTextViewNumber.setId(4);
        this.mTextViewNumber.setText(this.mImageNumber + ".");
        this.mTextViewNumber.setTextColor(-1);
        this.mTextViewNumber.setTextSize(17.0F);
        this.mContext.registerForContextMenu(this.mTextViewNumber);
        LayoutParams lp22 = new LayoutParams(-2, -2);
        lp22.addRule(10);
        lp22.addRule(9);
        lp22.leftMargin = 5;
        lp22.topMargin = 5;
        this.mTextViewNumber.setLayoutParams(lp22);
        this.mTextViewNumber.setOnClickListener(this.mOnClickListener);
        this.addView(this.mTextViewNumber);
    }

    public PanoramaViewGroup(GalleryActivity context, int number, String name, String path, Bitmap pano) {
        this(context, number, name, path);
        this.mImageBitmap = pano;
        this.mImageViewPano.setImageBitmap(this.mImageBitmap);
    }

    public int getImageNumber() {
        return this.mImageNumber;
    }

    public void setImageNumber(int number) {
        this.mImageNumber = number;
        if(this.mTextViewNumber != null) {
            this.mTextViewNumber.setText(this.mImageNumber + ".");
        }

    }

    public String getImagePath() {
        return this.mImagePath;
    }

    public String getImageName() {
        return this.mImageName;
    }

    public Bitmap getImageBitmap() {
        return this.mImageBitmap;
    }

    public void setImageBitmap(Bitmap pano) {
        this.mImageBitmap = pano;
        if(this.mImageViewPano != null) {
            this.mImageViewPano.setImageBitmap(this.mImageBitmap);
        }

    }

    public void releaseImageBitmap() {
        if(this.mImageViewPano != null) {
            this.mImageViewPano.setImageBitmap((Bitmap)null);
        }

        if(this.mImageBitmap != null) {
            this.mImageBitmap.recycle();
            this.mImageBitmap = null;
        }

    }

    public boolean hasImageBitmap() {
        return this.mImageBitmap != null;
    }

    public String getImageLabel() {
        return this.mImageLabel;
    }

    public void setImageLabel(String label) {
        this.mImageLabel = label;
        if(this.mTextViewName != null && this.mImageLabel != null) {
            this.mTextViewName.setText(this.mImageLabel);
        }

    }

    public void setOnClickListenerPano(PanoramaViewGroup.OnClickListenerPano listener) {
        this.mOnClickListenerPano = listener;
    }

    public interface OnClickListenerPano {
        void onClick(PanoramaViewGroup var1);
    }
}
