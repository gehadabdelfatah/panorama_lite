package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Handler.Callback;
import java.util.Iterator;
import java.util.List;

class SensorService {
    public static String TAG = "DMD_Library";
    private final int ROTATION_MATRIX_DATA_LENGTH;
    private final int QUATERNION_DATA_LENGTH;
    private final double ACCELEROMETER_TRANSFORMATION;
    private SensorService.SensorDataThread mSensorDataThread;
    private SensorManager mSensorManager;
    private Sensor mSensorGyroscope;
    private Sensor mSensorRotationVector;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorMagneticField;
    private SensorService.SensorServiceCallback mSensorServiceCallback;
    private Context mContext;
    private int mSensorDelayMode;
    private boolean mHasGyroscope;
    private boolean mHasCompass;
    private boolean mIsAllowedToTakeValues;
    private boolean mIsSensorsStarted;
    private double[] mGyroscopeData;
    private float[] mRotationVectorData;
    private float[] mAccelerometerData;
    private float[] mMagneticFieldData;
    private boolean mIsGyroscopeHasData;
    private boolean mIsRotationVectorHasData;
    private boolean mIsAccelerometerHasData;
    private boolean mIsMagneticFieldHasData;
    private SensorEventListener mSensorEventListener;

    public SensorService(Context context) {
        this(context, (SensorService.SensorServiceCallback)null);
    }

    public SensorService(Context context, SensorService.SensorServiceCallback sensorServiceCallback) {
        this.ROTATION_MATRIX_DATA_LENGTH = 9;
        this.QUATERNION_DATA_LENGTH = 4;
        this.ACCELEROMETER_TRANSFORMATION = -0.10224948875255624D;
        this.mSensorDelayMode = 0;
        this.mHasGyroscope = false;
        this.mHasCompass = false;
        this.mIsAllowedToTakeValues = false;
        this.mIsSensorsStarted = false;
        this.mGyroscopeData = new double[3];
        this.mRotationVectorData = new float[3];
        this.mAccelerometerData = new float[3];
        this.mMagneticFieldData = new float[3];
        this.mIsGyroscopeHasData = false;
        this.mIsRotationVectorHasData = false;
        this.mIsAccelerometerHasData = false;
        this.mIsMagneticFieldHasData = false;
        this.mSensorEventListener = new SensorEventListener() {
            public void onSensorChanged(SensorEvent event) {
                if(SensorService.this.mIsAllowedToTakeValues) {
                    int i;
                    if(SensorService.this.mHasGyroscope) {
                        if(event.sensor.getType() == 4) {
                            SensorService.this.mIsGyroscopeHasData = true;

                            for(i = 0; i < SensorService.this.mGyroscopeData.length; ++i) {
                                SensorService.this.mGyroscopeData[i] = (double)event.values[i];
                            }
                        } else if(event.sensor.getType() == 11) {
                            SensorService.this.mIsRotationVectorHasData = true;

                            for(i = 0; i < SensorService.this.mRotationVectorData.length; ++i) {
                                SensorService.this.mRotationVectorData[i] = event.values[i];
                            }
                        }

                        if(!SensorService.this.mIsGyroscopeHasData || !SensorService.this.mIsRotationVectorHasData) {
                            return;
                        }

                        float[] rotationMatrixData = new float[9];
                        float[] quaternionData = new float[4];
                        double[] rotationMatrixData2 = new double[9];
                        double[] quaternionData2 = new double[4];
                        SensorManager.getRotationMatrixFromVector(rotationMatrixData, SensorService.this.mRotationVectorData);
                        SensorManager.getQuaternionFromVector(quaternionData, SensorService.this.mRotationVectorData);

                        int ixx;
                        for(ixx = 0; ixx < rotationMatrixData.length; ++ixx) {
                            rotationMatrixData2[ixx] = (double)rotationMatrixData[ixx];
                        }

                        for(ixx = 0; ixx < quaternionData.length; ++ixx) {
                            quaternionData2[ixx] = (double)quaternionData[ixx];
                        }

                        double tmp = rotationMatrixData2[1];
                        rotationMatrixData2[1] = rotationMatrixData2[3];
                        rotationMatrixData2[3] = tmp;
                        tmp = rotationMatrixData2[2];
                        rotationMatrixData2[2] = rotationMatrixData2[6];
                        rotationMatrixData2[6] = tmp;
                        tmp = rotationMatrixData2[5];
                        rotationMatrixData2[5] = rotationMatrixData2[7];
                        rotationMatrixData2[7] = tmp;
                        if(SensorService.this.mSensorServiceCallback != null) {
                            float directionx = (float)(360.0D - Math.toDegrees(Math.atan2(rotationMatrixData2[6], -rotationMatrixData2[7]))) % 360.0F;
                            SensorService.this.mSensorServiceCallback.onGyroscopeValueUpdated(SensorService.this.mGyroscopeData, rotationMatrixData2, quaternionData2, directionx);
                        }
                    } else if(SensorService.this.mHasCompass) {
                        if(event.sensor.getType() == 1) {
                            SensorService.this.mIsAccelerometerHasData = true;

                            for(i = 0; i < SensorService.this.mAccelerometerData.length; ++i) {
                                SensorService.this.mAccelerometerData[i] = event.values[i];
                            }
                        } else if(event.sensor.getType() == 2) {
                            SensorService.this.mIsMagneticFieldHasData = true;

                            for(i = 0; i < SensorService.this.mMagneticFieldData.length; ++i) {
                                SensorService.this.mMagneticFieldData[i] = event.values[i];
                            }
                        }

                        if(!SensorService.this.mIsAccelerometerHasData || !SensorService.this.mIsMagneticFieldHasData) {
                            return;
                        }

                        double[] accelerometerData = new double[3];
                        double[] magneticFieldData = new double[3];

                        int ix;
                        for(ix = 0; ix < SensorService.this.mAccelerometerData.length; ++ix) {
                            accelerometerData[ix] = -0.10224948875255624D * (double)SensorService.this.mAccelerometerData[ix];
                        }

                        for(ix = 0; ix < SensorService.this.mMagneticFieldData.length; ++ix) {
                            magneticFieldData[ix] = (double)SensorService.this.mMagneticFieldData[ix];
                        }

                        float direction = 0.0F;
                        float[] R = new float[9];
                        float[] I = new float[9];
                        if(SensorManager.getRotationMatrix(R, I, SensorService.this.mAccelerometerData, SensorService.this.mMagneticFieldData) && R != null) {
                            direction = (float)(360.0D - Math.toDegrees(Math.atan2((double)R[2], (double)(-R[5])))) % 360.0F;
                        }

                        if(SensorService.this.mSensorServiceCallback != null) {
                            SensorService.this.mSensorServiceCallback.onCompassValueUpdated(accelerometerData, magneticFieldData, direction);
                        }
                    }

                }
            }

            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        this.mContext = context;
        this.mSensorServiceCallback = sensorServiceCallback;
        this.mSensorDataThread = new SensorService.SensorDataThread();
        this.mSensorDataThread.start();
        this.mSensorDataThread.mThreadHandler = new Handler(this.mSensorDataThread.getLooper(), this.mSensorDataThread);
        this.initializeSensors();
    }

    private void initializeSensors() {
        if(this.mContext != null) {
            this.mSensorManager = (SensorManager)this.mContext.getSystemService(Context.SENSOR_SERVICE);
            if(this.mSensorManager != null) {
                List<Sensor> listAllSensors = this.mSensorManager.getSensorList(-1);
                boolean hasGyro = false;
                boolean hasRotationVector = false;
                boolean hasAccelerometer = false;
                boolean hasMagneticField = false;
                Iterator var6 = listAllSensors.iterator();

                while(var6.hasNext()) {
                    Sensor sensor = (Sensor)var6.next();
                    switch(sensor.getType()) {
                        case 1:
                            hasAccelerometer = true;
                            break;
                        case 2:
                            hasMagneticField = true;
                            break;
                        case 4:
                            hasGyro = true;
                            break;
                        case 11:
                            hasRotationVector = true;
                    }
                }

                if(hasGyro && hasRotationVector) {
                    if(Globals.getNumCores() == 1) {
                        this.mSensorDelayMode = 1;
                    }

                    this.mHasGyroscope = true;
                }

                if(hasAccelerometer && hasMagneticField) {
                    this.mHasCompass = true;
                }

                if(this.mHasGyroscope || this.mHasCompass) {
                    if(this.mHasGyroscope) {
                        this.mSensorGyroscope = this.mSensorManager.getDefaultSensor(4);
                        this.mSensorRotationVector = this.mSensorManager.getDefaultSensor(11);
                    } else if(this.mHasCompass) {
                        this.mSensorAccelerometer = this.mSensorManager.getDefaultSensor(1);
                        this.mSensorMagneticField = this.mSensorManager.getDefaultSensor(2);
                    }

                }
            }
        }
    }

    public void startService() {
        if(!this.mIsSensorsStarted) {
            boolean isAccelerometerRegistered;
            boolean isMagneticFieldRegistered;
            if(this.mHasGyroscope) {
                isAccelerometerRegistered = this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensorGyroscope, this.mSensorDelayMode, this.mSensorDataThread.mThreadHandler);
                isMagneticFieldRegistered = this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensorRotationVector, this.mSensorDelayMode, this.mSensorDataThread.mThreadHandler);
                if(!isAccelerometerRegistered || !isMagneticFieldRegistered) {
                    if(isAccelerometerRegistered) {
                        this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorGyroscope);
                    }

                    if(isMagneticFieldRegistered) {
                        this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorRotationVector);
                    }

                    this.mHasGyroscope = false;
                    this.startService();
                }

                this.mIsAllowedToTakeValues = true;
            } else if(this.mHasCompass) {
                isAccelerometerRegistered = this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensorAccelerometer, this.mSensorDelayMode, this.mSensorDataThread.mThreadHandler);
                isMagneticFieldRegistered = this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensorMagneticField, this.mSensorDelayMode, this.mSensorDataThread.mThreadHandler);
                if(!isAccelerometerRegistered || !isMagneticFieldRegistered) {
                    if(isAccelerometerRegistered) {
                        this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorAccelerometer);
                    }

                    if(isMagneticFieldRegistered) {
                        this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorMagneticField);
                    }

                    this.mHasCompass = false;
                    return;
                }

                this.mIsAllowedToTakeValues = true;
            }

            this.mIsSensorsStarted = true;
        }
    }

    public void stopService() {
        if(this.mIsSensorsStarted) {
            this.mIsAllowedToTakeValues = false;
            if(this.mHasGyroscope) {
                this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorGyroscope);
                this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorRotationVector);
            } else if(this.mHasCompass) {
                this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorAccelerometer);
                this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensorMagneticField);
            }

            this.mIsSensorsStarted = false;
            this.mIsGyroscopeHasData = false;
            this.mIsRotationVectorHasData = false;
        }
    }

    public boolean isSensorsStarted() {
        return this.mIsSensorsStarted;
    }

    public boolean hasGyroscope() {
        return this.mHasGyroscope;
    }

    public class SensorDataThread extends HandlerThread implements Callback {
        Handler mThreadHandler;

        private SensorDataThread() {
            super("SensorDataThread", 10);
        }

        public boolean handleMessage(Message msg) {
            return false;
        }
    }

    public interface SensorServiceCallback {
        void onGyroscopeValueUpdated(double[] var1, double[] var2, double[] var3, float var4);

        void onCompassValueUpdated(double[] var1, double[] var2, float var3);
    }
}
