package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.GLES20;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;


import com.nativesystem.Core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.content.Context.MODE_PRIVATE;

public class DMD_Capture {
    private static String TAG = "DMD_Library";
    private int mImageViewWBID = 10;
    private int mImageViewExposureID = 11;
    private int mImageViewFlashID = 13;
    private boolean mIsCloseApp = false;
    private boolean mIsFirstLaunch = true;
    private boolean mIsCapturing;
    private boolean mIsFirstShot;
    private boolean mIsFocusing;
    private boolean mIsNotifiedCompassInterference = true;
    private boolean mIsCanStartShooting;
    private RelativeLayout mRelativeLayoutPreview;
    private LinearLayout mLinearLayoutIcons;
    static final String IMAGE_DIRECTORY_NAME = "Saved Images";

    private int[] mRGBA = null;
    private ViewerGLView mViewerGLView;
    private YinYangGLView mYinYangGLView;
    private ProgressBar mProgressBar;
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private Display mDisplay;
    private DisplayMetrics mDisplayMetrics;
    private SimpleDateFormat mSimpleDateFormat;
    private QuickAction mQuickActionWB;
    private WBManager mWbManager;
    private ImageView mImageViewWhiteBalance;
    private int mSelectedWBModeID = -1;
    private QuickAction mQuickActionExp;
    private ExposureManager mExposureManager;
    private ImageView mImageViewExposure;
    private int mSelectedExpModeID = -1;
    private QuickAction mQuickActionFlash;
    private FlashManager mFlashManager;
    private ImageView mImageViewFlash;
    private String mSelectedFlashModeID = "off";
    private SensorService mSensorService;
    private Handler mHandler;
    private int mCameraPreviewSizeWidth;
    private int mCameraPreviewSizeHeight;
    private int mCameraPictureSizeWidth;
    private int mCameraPictureSizeHeight;
    private float mDirection;
    private boolean mIsYinYangShown;
    private boolean mIsCameraControlsShown;
    private CallbackInterfaceShooter mCallbackInterface;
    private boolean mIsDeviceVertical;
    private boolean mIsReportDeviceVertical;
    private boolean mIsAutoFocusSupported;
    private boolean mIsExportOri;
    private String mOriFolderName = null;
    private String mEquiPath;
    private int mEquiWidth;
    private int mEquiHeight;
    private int mEquiMaxWidth;
    private float mEquiCenter;
    private SensorService.SensorServiceCallback mSensorServiceCallback = new SensorService.SensorServiceCallback() {
        public void onGyroscopeValueUpdated(double[] gyroData, double[] rotMatrixData, double[] quatData, float direction) {
            if (Globals.mIsCoreInitialized) {
                Core.updateGyro(gyroData, rotMatrixData, quatData);
                DMD_Capture.this.refreshStatus();
                DMD_Capture.this.mDirection = direction;
            }

        }

        public void onCompassValueUpdated(double[] accelData, double[] magFieldData, float direction) {
            if (Globals.mIsCoreInitialized) {
                Core.updateCompass(accelData, magFieldData, 1.0D);
                DMD_Capture.this.refreshStatus();
                DMD_Capture.this.mDirection = direction;
            }

        }
    };
    private AutoFocusCallback mAutoFocusCallback = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            DMD_Capture.this.mIsFocusing = false;
            if (DMD_Capture.this.mIsVertical) {
                DMD_Capture.this.mCamera.setOneShotPreviewCallback(DMD_Capture.this.mCaptureAnimationPreviewCallback);
            } else {
                DMD_Capture.this.finishShooting();
            }
            //setExportOriOn();
        }
    };
    private PreviewCallback mCaptureAnimationPreviewCallback = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            DMD_Capture.this.mCamera.takePicture(DMD_Capture.this.mShutterCallback, (PictureCallback) null, DMD_Capture.this.mJPEGPictureCallback);
            Core.decodeYUV420SP(DMD_Capture.this.mRGBA, data, DMD_Capture.this.mCameraPreviewSizeWidth, DMD_Capture.this.mCameraPreviewSizeHeight, !Globals.isTablet());
            Bitmap bitmapCameraCaptureEffect = null;

            try {
                bitmapCameraCaptureEffect = Bitmap.createBitmap(DMD_Capture.this.mRGBA, Globals.isTablet() ? DMD_Capture.this.mCameraPreviewSizeWidth : DMD_Capture.this.mCameraPreviewSizeHeight, Globals.isTablet() ? DMD_Capture.this.mCameraPreviewSizeHeight : DMD_Capture.this.mCameraPreviewSizeWidth, Config.ARGB_8888);
            } catch (OutOfMemoryError var5) {
                Log.d("on PreviewFrama", var5.getMessage());
            }

            if (DMD_Capture.this.mCallbackInterface != null) {
                DMD_Capture.this.mCallbackInterface.shotTakenPreviewReady(bitmapCameraCaptureEffect);
            }

        }
    };
    private ShutterCallback mShutterCallback = new ShutterCallback() {
        public void onShutter() {
            if (DMD_Capture.this.mCallbackInterface != null) {
                DMD_Capture.this.mCallbackInterface.takingPhoto();
            }

            Core.takingPhoto();
        }
    };
    private PictureCallback mJPEGPictureCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            if (camera != null) {
                Core.readExposure(data);
                Core.photoReady();
                ImageItem item = new ImageItem(data, DMD_Capture.this.mCameraPictureSizeWidth, DMD_Capture.this.mCameraPictureSizeHeight, Core.getr(), Core.getp(), Core.gety1(), Core.gety2());
                Globals.mListImageItems.offer(item);
                ++Globals.NumberShotImages;
                if (Globals.IS_STITCH_WHILE_SHOOTING) {
                    Globals.mIsStitchCurrentImage = true;
                    DMD_Capture.this.mViewerGLView.requestRender();
                }

                try {
                    camera.startPreview();
                } catch (Exception var5) {
                    Log.e(DMD_Capture.TAG, var5.getMessage());
                }

                DMD_Capture.this.mIsTakingPicture = false;
                if (DMD_Capture.this.mCallbackInterface != null) {
                    DMD_Capture.this.mCallbackInterface.photoTaken();
                }

                if (DMD_Capture.this.mIsExportOri) {
                    DMD_Capture.SaveOriAsyncTask saveOriAsyncTask = DMD_Capture.this.new SaveOriAsyncTask();
                    if (VERSION.SDK_INT < 11) {
                        saveOriAsyncTask.execute(new ImageItem[]{item});
                        saveOriAsyncTask.serData(data);

                    } else {
                        saveOriAsyncTask.serData(data);

                        saveOriAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ImageItem[]{item});
                    }
                }
            }

        }
    };
    private boolean mHasCalledFinishShooting = false;
    private int mResetReason = 0;
    private boolean mIsTakingPicture = false;
    private boolean mIsVertical = true;
    private Runnable mStopCapturingAndStitchRunnable = new Runnable() {
        public void run() {
            boolean finished = DMD_Capture.this.finishShooting();
        }
    };
    private Runnable mRunnableFinishedGeneratingImage = new Runnable() {
        public void run() {
            try {
                FileInputStream fis = new FileInputStream(DMD_Capture.this.mEquiPath + ".data");
                byte[] bWidth = new byte[4];
                byte[] bHeight = new byte[4];
                fis.read(bWidth, 0, 4);
                fis.read(bHeight, 0, 4);
                int width = Globals.byteArrayToInt(bWidth);
                int height = Globals.byteArrayToInt(bHeight);
                int bufferLength = width * 4;
                IntBuffer intBuffer = IntBuffer.allocate(width * height);
                byte[] bData = new byte[bufferLength];

                ByteBuffer bitmap;
                while (fis.read(bData, 0, bufferLength) > 0) {
                    for (int i = 0; i < bufferLength; i += 4) {
                        byte tmpR = bData[i + 3];
                        bData[i + 3] = bData[i + 2];
                        bData[i + 2] = bData[i + 1];
                        bData[i + 1] = bData[i];
                        bData[i] = tmpR;
                    }

                    bitmap = ByteBuffer.wrap(bData);
                    intBuffer.put(bitmap.asIntBuffer());
                }

                fis.close();
                Bitmap bitmapx = Bitmap.createBitmap(intBuffer.array(), height, width, Config.ARGB_8888);
                if (bitmapx != null) {
                    FileOutputStream fos = new FileOutputStream(DMD_Capture.this.mEquiPath, false);
                    bitmapx.compress(CompressFormat.JPEG, 100, fos);
                    fos.close();
                }

                bitmapx.recycle();
                bitmap = null;
                ExifInterface exifInterface = new ExifInterface(DMD_Capture.this.mEquiPath);
                exifInterface.setAttribute("Orientation", "6");
                exifInterface.saveAttributes();
            } catch (FileNotFoundException var11) {
                var11.printStackTrace();
            } catch (IOException var12) {
                var12.printStackTrace();
            }

            File fileEquiData = new File(DMD_Capture.this.mEquiPath + ".data");
            fileEquiData.delete();
            if (DMD_Capture.this.mCallbackInterface != null) {
                DMD_Capture.this.mCallbackInterface.onFinishGeneratingEqui();
            }

        }
    };
    private Runnable mCoreInitialized = new Runnable() {
        public void run() {
            Globals.mIsCoreInitialized = true;
        }
    };
    private Runnable mFinishedStitching = new Runnable() {
        public void run() {
            if (DMD_Capture.this.mCallbackInterface != null) {
                HashMap<String, Object> hashMap = new HashMap();
                hashMap.put(DMD_Capture.FinishShootingEnum.fovx.name(), Integer.valueOf(Globals.mFovX));
                DMD_Capture.this.mCallbackInterface.stitchingCompleted(hashMap);
            }

            if (DMD_Capture.this.mProgressBar.getParent() != null) {
                DMD_Capture.this.mRelativeLayoutPreview.removeView(DMD_Capture.this.mProgressBar);
            }

        }
    };
    private Runnable mFinishedReset = new Runnable() {
        public void run() {
            switch (DMD_Capture.this.mResetReason) {
                case 4:
                    DMD_Capture.this.startShooting((String) null);
                case 3:
                default:
                    DMD_Capture.this.mResetReason = 0;
            }
        }
    };
    private Runnable mRunnableUpdateIsVerticalDelegate = new Runnable() {
        public void run() {
            if (DMD_Capture.this.mCallbackInterface != null) {
                //here to set its vertical
                DMD_Capture.this.mCallbackInterface.deviceVerticalityChanged(DMD_Capture.this.mIsDeviceVertical ? 1 : 0);
            }

        }
    };
    private Runnable mRunnableReportCompassInterferenceToDelegate = new Runnable() {
        public void run() {
            if (DMD_Capture.this.mCallbackInterface != null) {
                HashMap<String, Object> hashMap = new HashMap();
                hashMap.put(DMD_Capture.CompassActionEnum.kDMDCompassInterference.name(), Boolean.TRUE);
                DMD_Capture.this.mCallbackInterface.compassEvent(hashMap);
            }

        }
    };
    private GLSurfaceView2.Renderer mRenderer = new GLSurfaceView2.Renderer() {
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            Core.init(4);
            Core.setF35(Globals.FOCAL_LENGTH);
            Core.setIsRotate(!Globals.isTablet());
            Core.set();
            DMD_Capture.this.mHandler.post(DMD_Capture.this.mCoreInitialized);
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
        }

        private void stitch() {
            if (!Globals.IS_STITCH_WHILE_SHOOTING) {
                if (Globals.mListImageItems == null || Globals.mListImageItems.size() < 2) {
                    return;
                }

                while (Globals.mListImageItems.size() > 0) {
                    ImageItem imageItem = (ImageItem) Globals.mListImageItems.poll();
                    byte[] data = imageItem.getData();
                    Options options = new Options();
                    //options.inPreferredConfig = Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                    int arraySize = bitmap.getRowBytes() * bitmap.getHeight();
                    ByteBuffer byteBuffer = ByteBuffer.allocate(arraySize);
                    bitmap.copyPixelsToBuffer(byteBuffer);
                    byte[] datax = null;
                    byteBuffer.rewind();
                    byte[] data2 = byteBuffer.array();
                    bitmap.recycle();
                    bitmap = null;
                    Core.loadImage(data2, imageItem.getWidth(), imageItem.getHeight(), imageItem.getRoll(), imageItem.getPitch(), imageItem.getYaw1(), imageItem.getYaw2());
                    byte[] data2x = null;
                    imageItem = null;
                    System.gc();
                }
            }

            Core.finishStitching();
            Globals.mListImageItems.clear();
        }

        public void onDrawFrame(GL10 gl) {
            if (Globals.mIsGenEqui) {
                Globals.mIsGenEqui = false;
                if (DMD_Capture.this.mEquiCenter == -1.0F) {
                    Core.genEqui(DMD_Capture.this.mEquiPath + ".data", DMD_Capture.this.mEquiHeight, DMD_Capture.this.mEquiWidth, DMD_Capture.this.mEquiMaxWidth);
                } else {
                    Core.genEqui1(DMD_Capture.this.mEquiPath + ".data", DMD_Capture.this.mEquiHeight, DMD_Capture.this.mEquiWidth, DMD_Capture.this.mEquiMaxWidth, DMD_Capture.this.mEquiCenter);
                }

                DMD_Capture.this.mHandler.post(DMD_Capture.this.mRunnableFinishedGeneratingImage);
            } else if (Globals.mIsReset) {
                Globals.mIsReset = false;
                Core.set();
                DMD_Capture.this.mHandler.post(DMD_Capture.this.mFinishedReset);
            } else {
                if (Globals.mIsTellEngineToStartStitching) {
                    Globals.mIsTellEngineToStartStitching = false;
                    Core.beginStitching(Globals.getPanoHeight(), Globals.FOVX_MAX);
                }

                if (Globals.mIsStitchCurrentImage) {
                    Globals.mIsStitchCurrentImage = false;
                    if (Globals.mListImageItems != null && Globals.mListImageItems.size() > 0) {
                        ImageItem imageItem = (ImageItem) Globals.mListImageItems.poll();
                        byte[] data = imageItem.getData();
                        Options options = new Options();
                        //options.inPreferredConfig = Config.ARGB_8888;
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                        int arraySize = bitmap.getRowBytes() * bitmap.getHeight();
                        ByteBuffer byteBuffer = ByteBuffer.allocate(arraySize);
                        bitmap.copyPixelsToBuffer(byteBuffer);
                        byte[] datax = null;
                        byteBuffer.rewind();
                        byte[] data2 = byteBuffer.array();
                        bitmap.recycle();
                        bitmap = null;
                        Core.loadImage(data2, imageItem.getWidth(), imageItem.getHeight(), imageItem.getRoll(), imageItem.getPitch(), imageItem.getYaw1(), imageItem.getYaw2());
                      //  byteBuffer = null;
                        byte[] data2x = null;
                        imageItem = null;
                        System.gc();
                    }

                    if (Globals.mListImageItems.size() > 0) {
                        Globals.mIsStitchCurrentImage = true;
                        DMD_Capture.this.mViewerGLView.requestRender();
                        return;
                    }
                }

                if (Globals.mIsStitch) {
                    this.stitch();
                    Globals.mFovX = Core.getFx();
                    Globals.mIsFirstDrawAfterStitch = true;
                    if (Globals.mIsStitch) {
                        Globals.mIsStitch = false;
                        DMD_Capture.this.mHandler.post(DMD_Capture.this.mFinishedStitching);
                    }

                } else if (Globals.mIsClear) {
                    GLES20.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
                    GLES20.glClear(16384);
                    Globals.mIsClear = false;
                }
            }
        }
    };

    public DMD_Capture() {
    }

    public ViewGroup initShooter(Context context, CallbackInterfaceShooter callbackInterface, int displayRotation, boolean showYinYang) {
        return this.initShooter(context, callbackInterface, displayRotation, showYinYang, false);
    }

    public ViewGroup initShooter(Context context, CallbackInterfaceShooter callbackInterface, int displayRotation, boolean showYinYang, boolean showCameraControls) {
        this.mCallbackInterface = callbackInterface;
        this.mIsYinYangShown = showYinYang;
        this.mIsCameraControlsShown = showCameraControls;
        this.mIsCapturing = false;
        this.mSimpleDateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
        this.mDisplay = ((Activity) context).getWindowManager().getDefaultDisplay();
        this.mDisplayMetrics = new DisplayMetrics();
        this.mDisplay.getMetrics(this.mDisplayMetrics);
        this.mIsAutoFocusSupported = context.getPackageManager().hasSystemFeature("android.hardware.camera.autofocus");
        if (displayRotation != 0 && displayRotation != 2) {
            Globals.SCREEN_WIDTH = this.mDisplayMetrics.heightPixels;
            Globals.SCREEN_HEIGHT = this.mDisplayMetrics.widthPixels;
        } else {
            Globals.SCREEN_WIDTH = this.mDisplayMetrics.widthPixels;
            Globals.SCREEN_HEIGHT = this.mDisplayMetrics.heightPixels;
        }

        Globals.SCREEN_ASPECT_RATIO = (double) Globals.SCREEN_HEIGHT / (double) Globals.SCREEN_WIDTH;
        this.mSensorService = new SensorService(context, this.mSensorServiceCallback);
        this.mRelativeLayoutPreview = new RelativeLayout(context);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        this.mRelativeLayoutPreview.setLayoutParams(params);
        this.mLinearLayoutIcons = new LinearLayout(context);
        LayoutParams params01;
        if (Globals.isTablet()) {
            this.mLinearLayoutIcons.setOrientation(LinearLayout.VERTICAL);
            params01 = new LayoutParams(-2, -2);
            params01.addRule(12);
            params01.addRule(11);
            this.mLinearLayoutIcons.setLayoutParams(params01);
        } else {
            this.mLinearLayoutIcons.setOrientation(LinearLayout.HORIZONTAL);
            params01 = new LayoutParams(-1, -2);
            params01.addRule(12);
            this.mLinearLayoutIcons.setLayoutParams(params01);
        }

        this.mCameraPreview = new CameraPreview(context, this.mCamera);
        this.mCameraPreview.setZOrderMediaOverlay(true);
        params01 = new LayoutParams(-1, -1);
        params01.addRule(14);
        params01.addRule(15);
        this.mCameraPreview.setLayoutParams(params01);
        this.mRelativeLayoutPreview.addView(this.mCameraPreview);
        this.mYinYangGLView = new YinYangGLView(context);
        this.mYinYangGLView.setIsGyroscope(this.mSensorService.hasGyroscope());
        this.mYinYangGLView.setZOrderOnTop(true);
        this.mYinYangGLView.setVisibility(this.mIsYinYangShown ? View.VISIBLE : View.GONE);
        int yinYangLeftRightMargin = (int) Math.round((double) Globals.SCREEN_WIDTH * 0.0625D);
        double yyRatio = Globals.isTablet() ? 0.13392857142857142D : 0.26785714285714285D;
        int yinYangHeight = (int) Math.round((double) (Globals.SCREEN_WIDTH - 2 * yinYangLeftRightMargin) * yyRatio);
        LayoutParams params1 = new LayoutParams(Globals.SCREEN_WIDTH - 2 * yinYangLeftRightMargin, yinYangHeight);
        params1.topMargin = (int) ((double) Globals.SCREEN_HEIGHT * 0.041666666666666664D);
        params1.addRule(10);
        params1.addRule(14);
        this.mYinYangGLView.setLayoutParams(params1);
        this.mRelativeLayoutPreview.addView(this.mYinYangGLView);
        this.mRelativeLayoutPreview.addView(this.mLinearLayoutIcons);
        this.mLinearLayoutIcons.setVisibility(this.mIsCameraControlsShown ? View.VISIBLE : View.GONE);
        this.mWbManager = new WBManager(context);
        this.mQuickActionWB = new QuickAction(context);
        this.mQuickActionWB.setAnimStyle(3);
        this.mImageViewWhiteBalance = new ImageView(context);
        this.mImageViewWhiteBalance.setId(this.mImageViewWBID);
        android.widget.LinearLayout.LayoutParams paramsb1 = new android.widget.LinearLayout.LayoutParams(-2, -2);
        if (!Globals.isTablet()) {
            paramsb1.weight = 1.0F;
        } else {
            paramsb1.bottomMargin = 10;
        }

        this.mImageViewWhiteBalance.setImageDrawable(context.getResources().getDrawable(Globals.getResourseIdByName(context.getPackageName(), "drawable", "wb_auto2")));
        this.mImageViewWhiteBalance.setLayoutParams(paramsb1);
        this.mQuickActionWB.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = source.getActionItem(pos);
                int i = 0;

                for (Iterator var6 = DMD_Capture.this.mQuickActionWB.getListActionItems().iterator(); var6.hasNext(); ++i) {
                    ActionItem item = (ActionItem) var6.next();
                    if (item.getActionId() != actionItem.getActionId()) {
                        item.setSelected(false);
                        ((RadioButton) DMD_Capture.this.mQuickActionWB.getListRadioButtons().get(i)).setChecked(false);
                    } else {
                        item.setSelected(true);
                        ((RadioButton) DMD_Capture.this.mQuickActionWB.getListRadioButtons().get(i)).setChecked(true);
                    }
                }

                if (DMD_Capture.this.mCameraPreview != null) {
                    DMD_Capture.this.mCameraPreview.setWhiteBalance(actionItem.getName());
                    DMD_Capture.this.mImageViewWhiteBalance.setImageDrawable(actionItem.getImage());
                    DMD_Capture.this.mSelectedWBModeID = actionItem.getActionId();
                }

            }
        });
        final int wbColor = context.getResources().getColor(Globals.getResourseIdByName(context.getPackageName(), "color", "color_wb_button_touched"));
        this.mImageViewWhiteBalance.setOnTouchListener(new OnTouchListener() {
            boolean isTouchInsideView;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        this.isTouchInsideView = true;
                        v.setBackgroundColor(wbColor);
                        break;
                    case 1:
                        if (this.isTouchInsideView) {
                            v.setBackgroundColor(0);
                            DMD_Capture.this.mQuickActionWB.show(v);
                        }
                        break;
                    case 2:
                        int x = (int) event.getX();
                        int y = (int) event.getY();
                        if (x < 0 || y < 0 || x >= v.getWidth() || y >= v.getHeight()) {
                            v.setBackgroundColor(0);
                            this.isTouchInsideView = false;
                        }
                }

                return true;
            }
        });
        this.mExposureManager = new ExposureManager(context);
        this.mQuickActionExp = new QuickAction(context);
        this.mQuickActionExp.setAnimStyle(3);
        this.mImageViewExposure = new ImageView(context);
        this.mImageViewExposure.setId(this.mImageViewExposureID);
        android.widget.LinearLayout.LayoutParams paramsb2 = new android.widget.LinearLayout.LayoutParams(-2, -2);
        if (!Globals.isTablet()) {
            paramsb2.weight = 1.0F;
        } else {
            paramsb2.bottomMargin = 10;
        }

        this.mImageViewExposure.setImageDrawable(context.getResources().getDrawable(Globals.getResourseIdByName(context.getPackageName(), "drawable", "exp_icon")));
        this.mImageViewExposure.setLayoutParams(paramsb2);
        this.mQuickActionExp.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = source.getActionItem(pos);
                int i = 0;

                for (Iterator var6 = DMD_Capture.this.mQuickActionExp.getListActionItems().iterator(); var6.hasNext(); ++i) {
                    ActionItem item = (ActionItem) var6.next();
                    if (item.getActionId() != actionItem.getActionId()) {
                        item.setSelected(false);
                        ((RadioButton) DMD_Capture.this.mQuickActionExp.getListRadioButtons().get(i)).setChecked(false);
                    } else {
                        item.setSelected(true);
                        ((RadioButton) DMD_Capture.this.mQuickActionExp.getListRadioButtons().get(i)).setChecked(true);
                    }
                }

                if (DMD_Capture.this.mCameraPreview != null) {
                    DMD_Capture.this.mCameraPreview.setAutoExposureLockStatus(actionItem.getActionId() == 1);
                    DMD_Capture.this.mSelectedExpModeID = actionItem.getActionId();
                }

            }
        });
        final int expColor = context.getResources().getColor(Globals.getResourseIdByName(context.getPackageName(), "color", "color_wb_button_touched"));
        this.mImageViewExposure.setOnTouchListener(new OnTouchListener() {
            boolean isTouchInsideView;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        this.isTouchInsideView = true;
                        v.setBackgroundColor(expColor);
                        break;
                    case 1:
                        if (this.isTouchInsideView) {
                            v.setBackgroundColor(0);
                            DMD_Capture.this.mQuickActionExp.show(v);
                        }
                        break;
                    case 2:
                        int x = (int) event.getX();
                        int y = (int) event.getY();
                        if (x < 0 || y < 0 || x >= v.getWidth() || y >= v.getHeight()) {
                            v.setBackgroundColor(0);
                            this.isTouchInsideView = false;
                        }
                }

                return true;
            }
        });
        this.mFlashManager = new FlashManager(context);
        this.mQuickActionFlash = new QuickAction(context);
        this.mQuickActionFlash.setAnimStyle(3);
        this.mImageViewFlash = new ImageView(context);
        this.mImageViewFlash.setId(this.mImageViewFlashID);
        android.widget.LinearLayout.LayoutParams paramsb3 = new android.widget.LinearLayout.LayoutParams(-2, -2);
        if (!Globals.isTablet()) {
            paramsb3.weight = 1.0F;
        } else {
            paramsb3.bottomMargin = 10;
        }

        this.mImageViewFlash.setImageDrawable(context.getResources().getDrawable(Globals.getResourseIdByName(context.getPackageName(), "drawable", "flash")));
        this.mImageViewFlash.setLayoutParams(paramsb3);
        this.mQuickActionFlash.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = source.getActionItem(pos);
                int i = 0;

                for (Iterator var6 = DMD_Capture.this.mQuickActionFlash.getListActionItems().iterator(); var6.hasNext(); ++i) {
                    ActionItem item = (ActionItem) var6.next();
                    if (item.getActionId() != actionItem.getActionId()) {
                        item.setSelected(false);
                        ((RadioButton) DMD_Capture.this.mQuickActionFlash.getListRadioButtons().get(i)).setChecked(false);
                    } else {
                        item.setSelected(true);
                        ((RadioButton) DMD_Capture.this.mQuickActionFlash.getListRadioButtons().get(i)).setChecked(true);
                    }
                }

                if (DMD_Capture.this.mCameraPreview != null) {
                    DMD_Capture.this.mCameraPreview.setFlashMode(actionItem.getName());
                    DMD_Capture.this.mSelectedFlashModeID = actionItem.getName();
                }

            }
        });
        final int flashColor = context.getResources().getColor(Globals.getResourseIdByName(context.getPackageName(), "color", "color_wb_button_touched"));
        this.mImageViewFlash.setOnTouchListener(new OnTouchListener() {
            boolean isTouchInsideView;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        this.isTouchInsideView = true;
                        v.setBackgroundColor(flashColor);
                        break;
                    case 1:
                        if (this.isTouchInsideView) {
                            v.setBackgroundColor(0);
                            DMD_Capture.this.mQuickActionFlash.show(v);
                        }
                        break;
                    case 2:
                        int x = (int) event.getX();
                        int y = (int) event.getY();
                        if (x < 0 || y < 0 || x >= v.getWidth() || y >= v.getHeight()) {
                            v.setBackgroundColor(0);
                            this.isTouchInsideView = false;
                        }
                }

                return true;
            }
        });
        this.mLinearLayoutIcons.addView(this.mImageViewWhiteBalance);
        this.mLinearLayoutIcons.addView(this.mImageViewExposure);
        this.mLinearLayoutIcons.addView(this.mImageViewFlash);
        Globals.ORI_FOLDER_NAME = context.getString(context.getApplicationInfo().labelRes);
        this.mHandler = new Handler();
        if (Globals.mListImageItems == null) {
            Globals.mListImageItems = new LinkedList();
        }

        this.mViewerGLView = Globals.getMyGLSurfaceView();
        if (this.mViewerGLView == null) {
            this.createViewer(context);
        }

        this.mViewerGLView.disableBufferSwap();
        this.mProgressBar = new ProgressBar(context, (AttributeSet) null, 16842874);
        LayoutParams params4 = new LayoutParams(-2, -2);
        params4.addRule(14);
        params4.addRule(15);
        this.mProgressBar.setLayoutParams(params4);
        if (this.mViewerGLView.getParent() == null) {
            this.mRelativeLayoutPreview.addView(this.mViewerGLView);
        }

        if (Globals.getNumCores() > 1) {
            Globals.IS_STITCH_WHILE_SHOOTING = true;
        }

        this.mRelativeLayoutPreview.setBackgroundColor(-65536);
        return this.mRelativeLayoutPreview;
    }

    private void createViewer(Context context) {
        this.mViewerGLView = new ViewerGLView(context, this.mRenderer);
        Globals.setMyGLSurfaceView(this.mViewerGLView);
        LayoutParams params3 = new LayoutParams(1, 1);
        params3.addRule(12);
        params3.addRule(11);
        this.mViewerGLView.setLayoutParams(params3);
        this.mViewerGLView.setRenderMode(0);
    }

    private void checkSurfaceView() {
        if (this.mViewerGLView != null) {
            if (this.mViewerGLView.getParent() != null) {
                RelativeLayout mll = (RelativeLayout) this.mViewerGLView.getParent();
                mll.removeView(this.mViewerGLView);
                this.mViewerGLView.setRenderer(this.mRenderer);
                LayoutParams params3 = new LayoutParams(1, 1);
                params3.addRule(12);
                params3.addRule(11);
                this.mViewerGLView.setLayoutParams(params3);
                this.mViewerGLView.setRenderMode(0);
                this.mViewerGLView.disableBufferSwap();
                this.mRelativeLayoutPreview.addView(this.mViewerGLView);
            } else {
                this.mViewerGLView.setRenderer(this.mRenderer);
                LayoutParams params3 = new LayoutParams(1, 1);
                params3.addRule(12);
                params3.addRule(11);
                this.mViewerGLView.setLayoutParams(params3);
                this.mViewerGLView.setRenderMode(0);
                this.mViewerGLView.disableBufferSwap();
                this.mRelativeLayoutPreview.addView(this.mViewerGLView);
            }
        }

    }

    public void restart() {
        if (!this.mIsCapturing) {
            if (Globals.mIsStitch && !Globals.IS_STITCH_WHILE_SHOOTING) {
                Globals.mIsStitch = false;
                this.resetCapturing(3);
                if (this.mProgressBar.getParent() != null) {
                    this.mRelativeLayoutPreview.removeView(this.mProgressBar);
                }
            }
        } else {
            this.resetCapturing(3);
        }

    }

    public void stopCamera() {
        this.mSensorService.stopService();
        this.mYinYangGLView.onPause();
        this.restart();
        if (this.mCamera != null) {
            this.mCameraPreview.stopPreview();
            this.mCamera.lock();
            this.mCamera.release();
            this.mCallbackInterface.onCameraStopped();
            this.mCameraPreview.setCamera((Camera) null);
            this.mCamera = null;
        }

        this.mRGBA = null;
    }

    public void startCamera(Context context, int width, int height) {
        Globals.mIsReset = true;
        if (this.mViewerGLView != null) {
            this.mViewerGLView.requestRender();
        }

        if (!this.mIsCloseApp) {
            boolean isCanUseCamera = false;
            if (this.mCamera == null) {
                try {
                    this.mCamera = Camera.open();
                    if (this.mCamera == null) {
                        throw new IllegalStateException();
                    }

                    isCanUseCamera = true;
                    this.mCallbackInterface.onCameraStarted();
                } catch (Exception var12) {

                }

                if (isCanUseCamera) {
                    if (this.mIsCameraControlsShown) {
                        this.mLinearLayoutIcons.setVisibility(View.VISIBLE);
                    }

                    this.mCameraPreview.setBackgroundColor(0);
                    this.mCameraPreview.setCamera(this.mCamera);
                    this.mCameraPreview.setCameraParameters();
                    this.mCameraPreview.surfaceChanged((SurfaceHolder) null, 0, 0, 0);
                    this.mCameraPreviewSizeWidth = this.mCamera.getParameters().getPreviewSize().width;
                    this.mCameraPreviewSizeHeight = this.mCamera.getParameters().getPreviewSize().height;
                    this.mCameraPictureSizeWidth = this.mCamera.getParameters().getPictureSize().width;
                    this.mCameraPictureSizeHeight = this.mCamera.getParameters().getPictureSize().height;
                    Globals.CAMERA_IMAGE_WIDTH = this.mCamera.getParameters().getPictureSize().width;
                    Globals.CAMERA_IMAGE_HEIGHT = this.mCamera.getParameters().getPictureSize().height;
                    double previewRatio = Globals.isTablet() ? (double) this.mCameraPreviewSizeHeight / (double) this.mCameraPreviewSizeWidth : (double) this.mCameraPreviewSizeWidth / (double) this.mCameraPreviewSizeHeight;
                    double parentViewRatio = (double) height / (double) width;
                    int previewLayoutWidth = width;
                    int previewLayoutHeight = height;
                    if (previewRatio < parentViewRatio) {
                        previewLayoutWidth = width;
                        previewLayoutHeight = (int) ((double) width * previewRatio);
                    } else if (previewRatio > parentViewRatio) {
                        previewLayoutHeight = height;
                        previewLayoutWidth = (int) ((double) height * (1.0D / previewRatio));
                    }

                    LayoutParams params = new LayoutParams(previewLayoutWidth, previewLayoutHeight);
                    params.addRule(14);
                    params.addRule(15);
                    this.mRelativeLayoutPreview.setLayoutParams(params);
                    if (this.mIsFirstLaunch) {
                        this.calculateDefaultFocalLength(this.mCameraPreview.getCameraMegaPixelCount());
                        new FocalLengthReader();
                        this.initializeWhiteBalanceControl(context);
                        this.initializeExposureControl(context);
                        this.initializeFlashControl(context);
                    }
                    if (this.mSelectedExpModeID == 1) {
                        this.mCameraPreview.setAutoExposureLockStatus(true);
                    }

                    if (this.mSelectedFlashModeID.equals("torch")) {
                        this.mCameraPreview.setFlashMode("torch");
                    }

                    if (this.mRGBA == null) {
                        this.mRGBA = new int[this.mCameraPreviewSizeWidth * this.mCameraPreviewSizeHeight];
                    }
                }
            }

            if (this.mViewerGLView == null) {
                this.mViewerGLView = Globals.getMyGLSurfaceView();
            }

            this.checkSurfaceView();
            if (this.mIsFirstLaunch && this.mViewerGLView != null) {
                this.mViewerGLView.onResume();
            }

            this.mIsFirstLaunch = false;
            this.mIsReportDeviceVertical = true;
            this.mSensorService.startService();
            this.mYinYangGLView.setRenderMode(1);
            this.mYinYangGLView.onResume();
        }

    }

    public int getSelectedPictureWidth() {
        return Globals.CAMERA_IMAGE_WIDTH;
    }

    public int getSelectedPictureHeight() {
        return Globals.CAMERA_IMAGE_HEIGHT;
    }

    public void setSafeZoneBounds(double lower_bound, double upper_bound) {
        Core.setSafeZoneBounds(lower_bound, upper_bound);
    }

    public void resetSafeZone() {
        Core.resetSafeZones();
    }

    public boolean startShooting(String path) {
       /* if (!this.mIsCanStartShooting) {
            return false;
        } else {*/
            Globals.FOVX_MAX = 180;
            this.startCapturing();
            return true;
      //  }
    }

    public boolean startShootingWithMaxFovX(int fovX) {
        if (!this.mIsCanStartShooting) {
            return false;
        } else {
            Globals.FOVX_MAX = Math.min(fovX, 180);
            this.startCapturing();
            return true;
        }
    }

    public boolean isTablet() {
        return Globals.isTablet();
    }

    public void stopShooting() {
        this.stopCamera();
    }

    public boolean finishShooting() {
        return this.stopCapturingAndStitch();
    }

    public void genEquiAt(String filename, int height, int width, int maxWidth, boolean logoZenith, boolean logoNadir) {
        this.mEquiPath = filename;
        this.mEquiHeight = height;
        this.mEquiWidth = width;
        this.mEquiMaxWidth = maxWidth;
        this.mEquiCenter = -1.0F;
        Globals.mIsGenEqui = true;
        this.mViewerGLView.requestRender();
    }

    public void genEquiAt(String filename, int height, int width, int maxWidth, float centerDegrees, boolean logoZenith, boolean logoNadir) {
        this.mEquiPath = filename;
        this.mEquiHeight = height;
        this.mEquiWidth = width;
        this.mEquiMaxWidth = maxWidth;
        this.mEquiCenter = centerDegrees;
        Globals.mIsGenEqui = true;
        this.mViewerGLView.requestRender();
    }

    public HashMap<String, Object> getIndicators() {
        HashMap<String, Object> hashMap = new HashMap();
        hashMap.put(DMD_Capture.ShootingIndicatorsEnum.pitch.name(), Double.valueOf(Globals.PITCH));
        hashMap.put(DMD_Capture.ShootingIndicatorsEnum.roll.name(), Double.valueOf(Globals.ROLL));
        hashMap.put(DMD_Capture.ShootingIndicatorsEnum.percentage.name(), Double.valueOf(Globals.PERCENTAGE));
        return hashMap;
    }

    public boolean isExposureSupported() {
        return this.mCameraPreview == null ? false : this.mCameraPreview.isAutoExposureLockSupported();
    }

    public void setExposureLocked() {
        if (this.mCameraPreview != null) {
            this.mCameraPreview.setAutoExposureLockStatus(true);
            this.mSelectedExpModeID = 1;
        }

    }

    public void setExposureAuto() {
        if (this.mCameraPreview != null) {
            this.mCameraPreview.setAutoExposureLockStatus(false);
            this.mSelectedExpModeID = 0;
        }

    }

    public void setExposureLockedOnFirst() {
        if (this.mCameraPreview != null) {
            this.mCameraPreview.setAutoExposureLockStatus(false);
            this.mSelectedExpModeID = 2;
        }

    }

    public boolean isFlashSupported() {
        return this.mCameraPreview == null ? false : this.mCameraPreview.isFlashModeAvailable("torch");
    }

    public void setFlashOn() {
        if (this.isFlashSupported()) {
            this.mCameraPreview.setFlashMode("torch");
        }

    }

    public void setFlashOff() {
        if (this.isFlashSupported()) {
            this.mCameraPreview.setFlashMode("off");
        }

    }

    public void setExportOriOn() {
        this.mIsExportOri = true;
    }

    public void setExportOriOff() {
        this.mIsExportOri = false;
    }

    public void setExportOriFolder(String foldername) {
        File folder = new File(foldername);
        if (folder.exists() && !folder.isDirectory()) {
            this.mOriFolderName = null;
        } else {
            this.mOriFolderName = foldername;
        }
    }

    private void calculateDefaultFocalLength(float mp) {
        if (mp >= 0.0F && (double) mp <= 2.5D) {
            Globals.FOCAL_LENGTH = 42.0D;
        } else if ((double) mp > 2.5D && mp <= 6.0F) {
            Globals.FOCAL_LENGTH = 35.0D;
        } else if (mp > 6.0F) {
            Globals.FOCAL_LENGTH = 30.0D;
        }

    }

    private void initializeWhiteBalanceControl(Context context) {
        boolean hasWBLock = this.mCameraPreview.isAutoWhiteBalanceLockSupported();
        String currentWBMode = this.mCameraPreview.getSelectedWhiteBalance();
        List<String> listWBModes = this.mCameraPreview.getListSupportedWhiteBalance();
        List<ActionItem> listWBActionItems = new ArrayList();
        ActionItem actionItem;
        if (hasWBLock) {
            this.mImageViewWhiteBalance.setImageDrawable(context.getResources().getDrawable(Globals.getResourseIdByName(context.getPackageName(), "drawable", "wb_locked2")));
            EffectItem wbItem = this.mWbManager.getLockedWBItem();
            actionItem = new ActionItem(wbItem.getID(), wbItem.getKey(), wbItem.getText(), wbItem.getIcon(), wbItem.getImage());
            actionItem.setPriority(wbItem.getPriority());
            actionItem.setSelected(true);
            listWBActionItems.add(actionItem);
            this.mSelectedWBModeID = actionItem.getActionId();
        }

        if (listWBModes != null) {
            for (int i = 0; i < listWBModes.size(); ++i) {
                String wbMode = (String) listWBModes.get(i);
                EffectItem wbItem = this.mWbManager.getWBItem(wbMode);
                if (wbItem != null) {
                    actionItem = new ActionItem(wbItem.getID(), wbItem.getKey(), wbItem.getText(), wbItem.getIcon(), wbItem.getImage());
                    actionItem.setPriority(wbItem.getPriority());
                    if (!hasWBLock && currentWBMode.equals(wbMode)) {
                        actionItem.setSelected(true);
                        this.mSelectedWBModeID = actionItem.getActionId();
                    }

                    listWBActionItems.add(actionItem);
                }
            }

            Collections.sort(listWBActionItems);
            Iterator var11 = listWBActionItems.iterator();

            while (var11.hasNext()) {
                actionItem = (ActionItem) var11.next();
                this.mQuickActionWB.addActionItem(actionItem);
            }
        }

    }

    private void initializeExposureControl(Context context) {
        boolean hasExpLock = this.mCameraPreview.isAutoExposureLockSupported();
        if (hasExpLock) {
            List<ActionItem> listExpActionItems = new ArrayList();
            List<EffectItem> listExpEffectItems = this.mExposureManager.getListExposureItems();
            this.mSelectedExpModeID = 0;

            for (int i = 0; i < listExpEffectItems.size(); ++i) {
                EffectItem expItem = (EffectItem) listExpEffectItems.get(i);
                ActionItem actionItem = new ActionItem(expItem.getID(), expItem.getKey(), expItem.getText(), expItem.getIcon(), expItem.getImage());
                actionItem.setPriority(expItem.getPriority());
                if (actionItem.getActionId() == this.mSelectedExpModeID) {
                    actionItem.setSelected(true);
                }

                listExpActionItems.add(actionItem);
            }

            Collections.sort(listExpActionItems);
            Iterator var8 = listExpActionItems.iterator();

            while (var8.hasNext()) {
                ActionItem actionItem = (ActionItem) var8.next();
                this.mQuickActionExp.addActionItem(actionItem);
            }
        } else if (this.mImageViewExposure.getParent() != null) {
            this.mLinearLayoutIcons.removeView(this.mImageViewExposure);
        }

    }

    private void initializeFlashControl(Context context) {
        boolean hasFlash = context.getPackageManager().hasSystemFeature("android.hardware.camera.flash");
        boolean hasTorch = this.mCameraPreview.isFlashModeAvailable("torch");
        List<String> listFlashModes = this.mCameraPreview.getListSupportedFlashModes();
        if (hasFlash && hasTorch && listFlashModes != null) {
            List<ActionItem> listFlashModeActionItems = new ArrayList();

            for (int i = 0; i < listFlashModes.size(); ++i) {
                String flashMode = (String) listFlashModes.get(i);
                EffectItem flashItem = this.mFlashManager.getFlashModeItem(flashMode);
                if (flashItem != null) {
                    ActionItem actionItem = new ActionItem(flashItem.getID(), flashItem.getKey(), flashItem.getText(), flashItem.getIcon(), flashItem.getImage());
                    actionItem.setPriority(flashItem.getPriority());
                    if (actionItem.getName().equals(this.mSelectedFlashModeID)) {
                        actionItem.setSelected(true);
                    }

                    listFlashModeActionItems.add(actionItem);
                }
            }

            Collections.sort(listFlashModeActionItems);
            Iterator var10 = listFlashModeActionItems.iterator();

            while (var10.hasNext()) {
                ActionItem actionItem = (ActionItem) var10.next();
                this.mQuickActionFlash.addActionItem(actionItem);
            }
        } else if (this.mImageViewFlash.getParent() != null) {
            this.mLinearLayoutIcons.removeView(this.mImageViewFlash);
        }

    }

    private void startCapturing() {
        if (this.mIsCameraControlsShown) {
            this.mLinearLayoutIcons.setVisibility(View.GONE);
        }

        if (this.mSelectedWBModeID == this.mWbManager.getLockedWBItem().getID() && this.mCameraPreview != null) {
            this.mCameraPreview.setAutoWhiteBalanceLockStatus(true);
        }

        if (this.mSelectedExpModeID != -1 && this.mCameraPreview != null && this.mSelectedExpModeID == 2) {
            this.mCameraPreview.setAutoExposureLockStatus(true);
        }

        this.mIsFirstShot = true;
        this.mIsCapturing = true;
        this.mIsNotifiedCompassInterference = false;
        this.mHasCalledFinishShooting = false;
        Globals.NumberShotImages = 0;
        Core.startShooting();
        Globals.mIsTellEngineToStartStitching = true;
        this.mViewerGLView.requestRender();
    }

    private boolean stopCapturingAndStitch() {
        if (this.mIsFocusing) {
            this.mIsFocusing = false;
            this.mCamera.cancelAutoFocus();
        }

        if (this.mSelectedWBModeID == this.mWbManager.getLockedWBItem().getID() && this.mCameraPreview != null) {
            this.mCameraPreview.setAutoWhiteBalanceLockStatus(false);
        }

        if (this.mSelectedExpModeID != -1 && this.mCameraPreview != null && this.mSelectedExpModeID == 2) {
            this.mCameraPreview.setAutoExposureLockStatus(false);
        }

        this.mIsCapturing = false;
        boolean finished = false;
        if (Globals.NumberShotImages >= 2) {
            this.switchView();
            finished = true;
        } else {
            this.resetCapturing(3);
            finished = false;
        }

        if (this.mCallbackInterface != null) {
            this.mCallbackInterface.shootingCompleted(finished);
        }

        return finished;
    }

    private void resetCapturing(int reason) {
        if (this.mIsCameraControlsShown) {
            this.mLinearLayoutIcons.setVisibility(View.VISIBLE);
        }

        this.mResetReason = reason;
        this.mIsCapturing = false;
        this.mIsTakingPicture = false;
        this.mIsReportDeviceVertical = true;
        Globals.NumberShotImages = 0;
        Globals.mListImageItems.clear();
        Globals.mIsReset = true;
        this.mViewerGLView.requestRender();
    }

    private void refreshStatus() {
        Globals.ROLL = Core.getRoll();
        Globals.PITCH = Core.getPitch();
        Globals.PERCENTAGE = Core.getPercentage();
        //this.mIsVertical = Core.deviceVertical();
        //this.mIsVertical = true;
     /*   if (this.mIsReportDeviceVertical || this.mIsVertical != this.mIsDeviceVertical) {
            this.mIsReportDeviceVertical = false;
            this.mIsDeviceVertical = this.mIsVertical;
            this.mHandler.post(this.mRunnableUpdateIsVerticalDelegate);
        }*/

        if (!this.mIsCapturing) {
            if (this.mIsVertical) {
                this.mIsCanStartShooting = true;
            } else {
                this.mIsCanStartShooting = false;
            }
        } else {
            boolean isCompassInterference = Core.CompassInterference();
            if (Core.isPreparingToShoot() && this.mCallbackInterface != null) {
                this.mCallbackInterface.preparingToShoot();
            }

            if (Core.cancelPreparingToShoot() && this.mCallbackInterface != null) {
                this.mCallbackInterface.canceledPreparingToShoot();
            }

            if (isCompassInterference || Core.getMaxFxReached() || Core.deviceHorizontal() && Globals.NumberShotImages >= 2) {
                if (isCompassInterference && !this.mIsNotifiedCompassInterference) {
                    this.mIsNotifiedCompassInterference = true;
                    this.mHandler.post(this.mRunnableReportCompassInterferenceToDelegate);
                }

                if (!this.mHasCalledFinishShooting) {
                    this.mHasCalledFinishShooting = true;
                    this.mHandler.post(this.mStopCapturingAndStitchRunnable);
                }

                return;
            }
            boolean canTakePhoto = Core.canTakePhoto();
            if (canTakePhoto && !this.mIsTakingPicture) {
                this.mIsTakingPicture = true;
                if (this.mIsFirstShot) {
                    this.mIsFirstShot = false;
                    Globals.LOCATION_FIRST_SHOT_DIRECTION = this.mDirection;
                    if (this.mIsAutoFocusSupported) {
                        this.mIsFocusing = true;
                        this.mCamera.autoFocus(this.mAutoFocusCallback);
                    } else if (this.mIsVertical) {
                        this.mCamera.setOneShotPreviewCallback(this.mCaptureAnimationPreviewCallback);
                    } else {
                        this.finishShooting();
                    }
                } else {
                    this.mCamera.setOneShotPreviewCallback(this.mCaptureAnimationPreviewCallback);
                }
            }
        }

    }

    private void switchView() {
        this.mCameraPreview.stopPreview();
        this.mSensorService.stopService();
        this.mIsCanStartShooting = false;
        this.mYinYangGLView.setRenderMode(0);
        this.mYinYangGLView.resetParameters();
        this.mYinYangGLView.requestRender();
        this.mCameraPreview.setBackgroundColor(-16777216);
        if (this.mProgressBar.getParent() != null) {
            this.mRelativeLayoutPreview.removeView(this.mProgressBar);
        }

        this.mRelativeLayoutPreview.addView(this.mProgressBar);
        Globals.mIsStitch = true;
        this.mViewerGLView.requestRender();
    }

    private String decimalToDMS(double coord) {
        double mod = coord % 1.0D;
        int intPart = (int) coord;
        String degrees = String.valueOf(intPart);
        coord = mod * 60.0D;
        mod = coord % 1.0D;
        intPart = (int) coord;
        if (intPart < 0) {
            intPart *= -1;
        }

        String minutes = String.valueOf(intPart);
        coord = mod * 60.0D;
        intPart = (int) coord;
        if (intPart < 0) {
            intPart *= -1;
        }

        String seconds = String.valueOf(intPart);
        String output = degrees + "/1," + minutes + "/1," + seconds + "/1";
        return output;
    }

    private class SaveOriAsyncTask extends AsyncTask<ImageItem, Void, Void> {
        private String filePath;
        private byte[] photo;

        private SaveOriAsyncTask() {
        }

        protected void onPreExecute() {
         /*   String name = "shot-" + DMD_Capture.this.mSimpleDateFormat.format(new Date()) + ".jpg";
            if(DMD_Capture.this.mOriFolderName != null) {
                this.filePath = DMD_Capture.this.mOriFolderName + "/" + name;
            } else {
                this.filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/" + Globals.ORI_FOLDER_NAME + "/" + name;
                //this.filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
               // saveImageToExternalStorage()
            }*/

        }

        public void serData(byte[] photo) {
            this.photo = photo;
        }

        public void saveImageToExternalStorage(Bitmap finalBitmap/*, Activity activity*/) {
            ContextWrapper wrapper;
            File myDir = null;
            String root;
            Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
            Boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();

            if (/*isSDSupportedDevice &&*/ isSDPresent) {// yes SD-card is present
                root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                myDir = new File(root, IMAGE_DIRECTORY_NAME);
            } else {
                // Sorry
                //  wrapper = new ContextWrapper(activity);
                // myDir = wrapper.getDir(IMAGE_DIRECTORY_NAME, MODE_PRIVATE);
            }
            // Create a media file name
            // Create the storage directory if it does not exist
            if (!myDir.exists()) {
                if (!myDir.mkdirs()) {
                    android.util.Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            String fname = "Image-" + timeStamp + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                this.filePath = Uri.fromFile(file).getPath();
                //  finalBitmap.setDensity(100);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        protected Void doInBackground(ImageItem... params) {
            ImageItem imageItem = params[0];

            try {
               /* FileOutputStream fos = new FileOutputStream(this.filePath);
                fos.write(imageItem.getData());
                fos.close();
                if(!Globals.isTablet()) {
                    ExifInterface exif = new ExifInterface(this.filePath);
                    exif.setAttribute("Orientation", "6");
                    exif.saveAttributes();
                }*/
                //  saveImageToExternalStorage(BitmapFactory.decodeByteArray(imageItem.getData(),0,imageItem.getData().length));
                saveImageToExternalStorage(BitmapFactory.decodeByteArray(photo, 0, photo.length));
                if (!Globals.isTablet()) {
                    ExifInterface exif = new ExifInterface(this.filePath);
                    exif.setAttribute("Orientation", "6");
                    exif.saveAttributes();
                }
            } catch (Exception var5) {
                Log.d("donbak", var5.getMessage());
            }

            return null;
        }

        protected void onPostExecute(Void result) {
        }
    }

    public static enum FinishShootingEnum {
        fovx;

        private FinishShootingEnum() {
        }
    }

    public static enum ShootingIndicatorsEnum {
        pitch,
        roll,
        percentage;

        private ShootingIndicatorsEnum() {
        }
    }

    public static enum CompassActionEnum {
        kDMDCompassInterference;

        private CompassActionEnum() {
        }
    }
}
