package com.android.camera.dmd_pano_librarylite.dermandar.gallery;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

class MyScrollView extends ScrollView {
    private int mLastYPosition;
    private int mCheckDelay = 100;
    private boolean mIsScrollMonitorAssigned = false;
    private Runnable mScrollMonitor;
    private MyScrollView.ScrollViewListener mScrollViewListener = null;

    public MyScrollView(Context context) {
        super(context);
        this.initialize();
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initialize();
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.initialize();
    }

    private void initialize() {
        this.mScrollMonitor = new Runnable() {
            public void run() {
                int newYPosition = MyScrollView.this.getScrollY();
                if(newYPosition - MyScrollView.this.mLastYPosition == 0) {
                    if(MyScrollView.this.mScrollViewListener != null) {
                        MyScrollView.this.mScrollViewListener.onScrollEnded();
                    }

                    MyScrollView.this.mIsScrollMonitorAssigned = false;
                } else {
                    MyScrollView.this.mLastYPosition = MyScrollView.this.getScrollY();
                    MyScrollView.this.postDelayed(MyScrollView.this.mScrollMonitor, (long)MyScrollView.this.mCheckDelay);
                }

            }
        };
    }

    public void setScrollViewListener(MyScrollView.ScrollViewListener scrollViewListener) {
        this.mScrollViewListener = scrollViewListener;
    }

    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if(!this.mIsScrollMonitorAssigned) {
            this.mLastYPosition = this.getScrollY();
            this.postDelayed(this.mScrollMonitor, (long)this.mCheckDelay);
            this.mIsScrollMonitorAssigned = true;
        }

        if(this.mScrollViewListener != null) {
            this.mScrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
        }

    }

    public interface ScrollViewListener {
        void onScrollChanged(MyScrollView var1, int var2, int var3, int var4, int var5);

        void onScrollEnded();
    }
}
