package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.graphics.drawable.Drawable;

class EffectItem {
    private int mID;
    private Drawable mIcon;
    private Drawable mImage;
    private String mKey;
    private String mText;
    private int mPriority;

    public EffectItem(int id, String key, String text, Drawable icon, Drawable image, int priority) {
        this.mID = id;
        this.mKey = key;
        this.mText = text;
        this.mIcon = icon;
        this.mImage = image;
        this.mPriority = priority;
    }

    public int getID() {
        return this.mID;
    }

    public Drawable getIcon() {
        return this.mIcon;
    }

    public Drawable getImage() {
        return this.mImage;
    }

    public void setIcon(Drawable icon) {
        this.mIcon = icon;
    }

    public String getKey() {
        return this.mKey;
    }

    public String getText() {
        return this.mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public int getPriority() {
        return this.mPriority;
    }

    public void setPriority(int priority) {
        this.mPriority = priority;
    }
}
