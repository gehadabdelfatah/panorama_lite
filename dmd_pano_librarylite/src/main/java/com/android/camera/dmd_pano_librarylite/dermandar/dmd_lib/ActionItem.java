package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

class ActionItem implements Comparable<ActionItem> {
    private Drawable icon;
    private Drawable image;
    private Bitmap thumb;
    private String name;
    private String title;
    private int actionId;
    private boolean selected;
    private boolean sticky;
    private int priority;

    public ActionItem(int actionId, String name, String title, Drawable icon, Drawable image) {
        this.actionId = -1;
        this.sticky = true;
        this.priority = 2147483647;
        this.name = name;
        this.title = title;
        this.icon = icon;
        this.actionId = actionId;
        this.image = image;
    }

    public ActionItem() {
        this(-1, (String)null, (String)null, (Drawable)null, (Drawable)null);
    }

    public ActionItem(int actionId, String name, String title) {
        this(actionId, name, title, (Drawable)null, (Drawable)null);
    }

    public ActionItem(Drawable icon) {
        this(-1, (String)null, (String)null, icon, (Drawable)null);
    }

    public ActionItem(int actionId, Drawable icon) {
        this(actionId, (String)null, (String)null, icon, (Drawable)null);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public Drawable getImage() {
        return this.image;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public int getActionId() {
        return this.actionId;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public boolean isSticky() {
        return this.sticky;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setThumb(Bitmap thumb) {
        this.thumb = thumb;
    }

    public Bitmap getThumb() {
        return this.thumb;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return this.priority;
    }

    public int compareTo(ActionItem another) {
        return another != null && this.priority >= another.priority?(this.priority > another.priority?-1:0):1;
    }
}
