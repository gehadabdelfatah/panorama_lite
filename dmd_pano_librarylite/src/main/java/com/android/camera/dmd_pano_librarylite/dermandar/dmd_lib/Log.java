package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

import com.android.camera.dmd_pano_librarylite.BuildConfig;

/**
 * Created by Pc1 on 05/02/2018.
 */


class Log {
    private static boolean LOG_ENABLED;

    Log() {
    }

    public static void v(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.v(TAG, msg);
        }

    }

    public static void d(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.d(TAG, msg);
        }

    }

    public static void e(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.e(TAG, msg);
        }

    }

    public static void i(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.i(TAG, msg);
        }

    }

    public static void w(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.w(TAG, msg);
        }

    }

    public static void wtf(String TAG, String msg) {
        if(LOG_ENABLED) {
            android.util.Log.wtf(TAG, msg);
        }

    }

    static {
        LOG_ENABLED = BuildConfig.DEBUG;
    }
}
