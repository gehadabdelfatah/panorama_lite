package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class WBManager {
    private List<EffectItem> mListWBItems;
    private EffectItem mLockedWBItem;
    private Context mContext;

    public WBManager(Context context) {
        this.mContext = context;
        this.mLockedWBItem = new EffectItem(0, "auto", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_locked_start")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_locked")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_locked2")), 0);
        this.mListWBItems = new ArrayList();
        this.mListWBItems.add(new EffectItem(1, "auto", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_auto")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_auto")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_auto2")), 1));
        this.mListWBItems.add(new EffectItem(2, "cloudy-daylight", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_cloudy_daylight")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_cloudy")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_cloudy2")), 2));
        this.mListWBItems.add(new EffectItem(3, "daylight", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_daylight")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_daylight")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_daylight2")), 3));
        this.mListWBItems.add(new EffectItem(4, "fluorescent", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_fluorescent")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_fluorescent")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_fluorescent2")), 4));
        this.mListWBItems.add(new EffectItem(5, "incandescent", this.mContext.getString(Globals.getResourseIdByName(this.mContext.getPackageName(), "string", "wb_incandescent")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "ic_menuselect_wb_incandescent")), this.mContext.getResources().getDrawable(Globals.getResourseIdByName(this.mContext.getPackageName(), "drawable", "wb_incandescent2")), 5));
    }

    public EffectItem getLockedWBItem() {
        return this.mLockedWBItem;
    }

    public EffectItem getWBItem(String key) {
        Iterator var2 = this.mListWBItems.iterator();

        EffectItem item;
        do {
            if(!var2.hasNext()) {
                return null;
            }

            item = (EffectItem)var2.next();
        } while(!item.getKey().equals(key));

        return item;
    }
}
