package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */
import android.content.Context;
import android.opengl.GLDebugHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;
import java.io.Writer;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

class GLSurfaceView2 extends SurfaceView implements Callback {
    private static final boolean LOG_THREADS = false;
    private static final boolean LOG_PAUSE_RESUME = false;
    private static final boolean LOG_SURFACE = true;
    private static final boolean LOG_RENDERER = false;
    private static final boolean LOG_RENDERER_DRAW_FRAME = false;
    private static final boolean LOG_EGL = false;
    private static final boolean DRAW_TWICE_AFTER_SIZE_CHANGED = true;
    public static final int RENDERMODE_WHEN_DIRTY = 0;
    public static final int RENDERMODE_CONTINUOUSLY = 1;
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final GLSurfaceView2.GLThreadManager sGLThreadManager = new GLSurfaceView2.GLThreadManager();
    private boolean mSizeChanged = true;
    private GLSurfaceView2.GLThread mGLThread;
    private GLSurfaceView2.EGLConfigChooser mEGLConfigChooser;
    private GLSurfaceView2.EGLContextFactory mEGLContextFactory;
    private GLSurfaceView2.EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private GLSurfaceView2.GLWrapper mGLWrapper;
    private int mDebugFlags;
    private int mEGLContextClientVersion;

    public GLSurfaceView2(Context context) {
        super(context);
        this.init();
    }

    public GLSurfaceView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init();
    }

    private void init() {
        SurfaceHolder holder = this.getHolder();
        holder.addCallback(this);
        holder.setFormat(4);
        holder.setType(2);
    }

    public void setGLWrapper(GLSurfaceView2.GLWrapper glWrapper) {
        this.mGLWrapper = glWrapper;
    }

    public void setDebugFlags(int debugFlags) {
        this.mDebugFlags = debugFlags;
    }

    public int getDebugFlags() {
        return this.mDebugFlags;
    }

    public void setRenderer(GLSurfaceView2.Renderer renderer) {
        if(this.mEGLConfigChooser == null) {
            this.mEGLConfigChooser = new GLSurfaceView2.SimpleEGLConfigChooser(true);
        }

        if(this.mEGLContextFactory == null) {
            this.mEGLContextFactory = new GLSurfaceView2.DefaultContextFactory();
        }

        if(this.mEGLWindowSurfaceFactory == null) {
            this.mEGLWindowSurfaceFactory = new GLSurfaceView2.DefaultWindowSurfaceFactory();
        }

        if(this.mGLThread == null) {
            this.mGLThread = new GLSurfaceView2.GLThread(renderer);
            this.mGLThread.start();
        } else {
            this.mGLThread.setRenderer(renderer);
        }

    }

    public void setEGLContextFactory(GLSurfaceView2.EGLContextFactory factory) {
        this.checkRenderThreadState();
        this.mEGLContextFactory = factory;
    }

    public void setEGLWindowSurfaceFactory(GLSurfaceView2.EGLWindowSurfaceFactory factory) {
        this.checkRenderThreadState();
        this.mEGLWindowSurfaceFactory = factory;
    }

    public void setEGLConfigChooser(GLSurfaceView2.EGLConfigChooser configChooser) {
        this.checkRenderThreadState();
        this.mEGLConfigChooser = configChooser;
    }

    public void setEGLConfigChooser(boolean needDepth) {
        this.setEGLConfigChooser(new GLSurfaceView2.SimpleEGLConfigChooser(needDepth));
    }

    public void setEGLConfigChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
        this.setEGLConfigChooser(new GLSurfaceView2.ComponentSizeChooser(redSize, greenSize, blueSize, alphaSize, depthSize, stencilSize));
    }

    public void setEGLContextClientVersion(int version) {
        this.checkRenderThreadState();
        this.mEGLContextClientVersion = version;
    }

    public void setRenderMode(int renderMode) {
        this.mGLThread.setRenderMode(renderMode);
    }

    public int getRenderMode() {
        return this.mGLThread.getRenderMode();
    }

    public void requestRender() {
        this.mGLThread.requestRender();
    }

    public void disableBufferSwap() {
        this.mGLThread.setBufferSwap(false);
    }

    public void enableBufferSwap() {
        this.mGLThread.setBufferSwap(true);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        this.mGLThread.onWindowResize(w, h);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable r) {
        this.mGLThread.queueEvent(r);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void checkRenderThreadState() {
        if(this.mGLThread != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    private static class GLThreadManager {
        private static String TAG = "GLThreadManager";
        private boolean mGLESVersionCheckComplete;
        private int mGLESVersion;
        private boolean mGLESDriverCheckComplete;
        private boolean mMultipleGLESContextsAllowed;
        private static final int kGLES_20 = 131072;
        private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
        private GLSurfaceView2.GLThread mEglOwner;

        private GLThreadManager() {
        }

        public synchronized void threadExiting(GLSurfaceView2.GLThread thread) {
            thread.mExited = true;
            if(this.mEglOwner == thread) {
                this.mEglOwner = null;
            }

            this.notifyAll();
        }

        public boolean tryAcquireEglContextLocked(GLSurfaceView2.GLThread thread) {
            if(this.mEglOwner != thread && this.mEglOwner != null) {
                this.checkGLESVersion();
                return this.mMultipleGLESContextsAllowed;
            } else {
                this.mEglOwner = thread;
                this.notifyAll();
                return true;
            }
        }

        public void releaseEglContextLocked(GLSurfaceView2.GLThread thread) {
            if(this.mEglOwner == thread) {
                this.mEglOwner = null;
            }

            this.notifyAll();
        }

        public synchronized boolean shouldReleaseEGLContextWhenPausing() {
            return false;
        }

        public synchronized boolean shouldTerminateEGLWhenPausing() {
            this.checkGLESVersion();
            return !this.mMultipleGLESContextsAllowed;
        }

        public synchronized void checkGLDriver(GL10 gl) {
            if(!this.mGLESDriverCheckComplete) {
                this.checkGLESVersion();
                if(this.mGLESVersion < 131072) {
                    String renderer = gl.glGetString(7937);
                    this.mMultipleGLESContextsAllowed = !renderer.startsWith("Q3Dimension MSM7500 ");
                    Log.w(TAG, "checkGLDriver renderer = \"" + renderer + "\" multipleContextsAllowed = " + this.mMultipleGLESContextsAllowed);
                    this.notifyAll();
                }

                this.mGLESDriverCheckComplete = true;
            }

        }

        private void checkGLESVersion() {
            if(!this.mGLESVersionCheckComplete) {
                this.mGLESVersion = 0;
                if(this.mGLESVersion >= 131072) {
                    this.mMultipleGLESContextsAllowed = true;
                }

                Log.w(TAG, "checkGLESVersion mGLESVersion = " + this.mGLESVersion + " mMultipleGLESContextsAllowed = " + this.mMultipleGLESContextsAllowed);
                this.mGLESVersionCheckComplete = true;
            }

        }
    }

    static class LogWriter extends Writer {
        private StringBuilder mBuilder = new StringBuilder();

        LogWriter() {
        }

        public void close() {
            this.flushBuilder();
        }

        public void flush() {
            this.flushBuilder();
        }

        public void write(char[] buf, int offset, int count) {
            for(int i = 0; i < count; ++i) {
                char c = buf[offset + i];
                if(c == 10) {
                    this.flushBuilder();
                } else {
                    this.mBuilder.append(c);
                }
            }

        }

        private void flushBuilder() {
            if(this.mBuilder.length() > 0) {
                Log.v("GLSurfaceView", this.mBuilder.toString());
                this.mBuilder.delete(0, this.mBuilder.length());
            }

        }
    }

    class GLThread extends Thread {
        private boolean mShouldExit;
        private boolean mExited;
        private boolean mRequestPaused;
        private boolean mPaused;
        private boolean mHasSurface;
        private boolean mWaitingForSurface;
        private boolean mHaveEglContext;
        private boolean mHaveEglSurface;
        private int mWidth = 0;
        private int mHeight = 0;
        private int mRenderMode = 1;
        private boolean mRequestRender = true;
        private boolean mBufferSwap = true;
        private boolean mRenderComplete;
        private ArrayList<Runnable> mEventQueue = new ArrayList();
        private GLSurfaceView2.Renderer mRenderer;
        private GLSurfaceView2.EglHelper mEglHelper;

        GLThread(GLSurfaceView2.Renderer renderer) {
            this.mRenderer = renderer;
        }

        public void run() {
            this.setName("GLThread " + this.getId());

            try {
                this.guardedRun();
            } catch (InterruptedException var5) {
                ;
            } finally {
                GLSurfaceView2.sGLThreadManager.threadExiting(this);
            }

        }

        private void stopEglSurfaceLocked() {
            if(this.mHaveEglSurface) {
                this.mHaveEglSurface = false;
                this.mEglHelper.destroySurface();
            }

        }

        private void stopEglContextLocked() {
            if(this.mHaveEglContext) {
                this.mEglHelper.finish();
                this.mHaveEglContext = false;
                GLSurfaceView2.sGLThreadManager.releaseEglContextLocked(this);
            }

        }

        private void guardedRun() throws InterruptedException {
            this.mEglHelper = GLSurfaceView2.this.new EglHelper();
            this.mHaveEglContext = false;
            this.mHaveEglSurface = false;
            boolean var25 = false;

            label347: {
                try {
                    var25 = true;
                    GL10 gl = null;
                    boolean createEglContext = false;
                    boolean createEglSurface = false;
                    boolean lostEglContext = false;
                    boolean sizeChanged = false;
                    boolean wantRenderNotification = false;
                    boolean doRenderNotification = false;
                    int w = 0;
                    int h = 0;
                    Runnable event = null;

                    label340:
                    while(true) {
                        synchronized(GLSurfaceView2.sGLThreadManager) {
                            while(true) {
                                if(this.mShouldExit) {
                                    var25 = false;
                                    break label340;
                                }

                                if(!this.mEventQueue.isEmpty()) {
                                    event = (Runnable)this.mEventQueue.remove(0);
                                    break;
                                }

                                if(this.mPaused != this.mRequestPaused) {
                                    this.mPaused = this.mRequestPaused;
                                    GLSurfaceView2.sGLThreadManager.notifyAll();
                                }

                                if(lostEglContext) {
                                    this.stopEglSurfaceLocked();
                                    this.stopEglContextLocked();
                                    lostEglContext = false;
                                }

                                if(this.mHaveEglSurface && this.mPaused) {
                                    Log.i("GLThread", "releasing EGL surface because paused tid=" + this.getId());
                                    this.stopEglSurfaceLocked();
                                    if(GLSurfaceView2.sGLThreadManager.shouldReleaseEGLContextWhenPausing()) {
                                        this.stopEglContextLocked();
                                        Log.i("GLThread", "releasing EGL context because paused tid=" + this.getId());
                                    }

                                    if(GLSurfaceView2.sGLThreadManager.shouldTerminateEGLWhenPausing()) {
                                        this.mEglHelper.finish();
                                        Log.i("GLThread", "terminating EGL because paused tid=" + this.getId());
                                    }
                                }

                                if(!this.mHasSurface && !this.mWaitingForSurface) {
                                    Log.i("GLThread", "noticed surfaceView surface lost tid=" + this.getId());
                                    if(this.mHaveEglSurface) {
                                        this.stopEglSurfaceLocked();
                                    }

                                    this.mWaitingForSurface = true;
                                    GLSurfaceView2.sGLThreadManager.notifyAll();
                                }

                                if(this.mHasSurface && this.mWaitingForSurface) {
                                    Log.i("GLThread", "noticed surfaceView surface acquired tid=" + this.getId());
                                    this.mWaitingForSurface = false;
                                    GLSurfaceView2.sGLThreadManager.notifyAll();
                                }

                                if(doRenderNotification) {
                                    wantRenderNotification = false;
                                    doRenderNotification = false;
                                    this.mRenderComplete = true;
                                    GLSurfaceView2.sGLThreadManager.notifyAll();
                                }

                                if(!this.mPaused && this.mHasSurface && this.mWidth > 0 && this.mHeight > 0 && (this.mRequestRender || this.mRenderMode == 1)) {
                                    if(!this.mHaveEglContext && GLSurfaceView2.sGLThreadManager.tryAcquireEglContextLocked(this)) {
                                        try {
                                            this.mEglHelper.start();
                                        } catch (RuntimeException var29) {
                                            GLSurfaceView2.sGLThreadManager.releaseEglContextLocked(this);
                                            throw var29;
                                        }

                                        this.mHaveEglContext = true;
                                        createEglContext = true;
                                        GLSurfaceView2.sGLThreadManager.notifyAll();
                                    }

                                    if(this.mHaveEglContext && !this.mHaveEglSurface) {
                                        this.mHaveEglSurface = true;
                                        createEglSurface = true;
                                        sizeChanged = true;
                                    }

                                    if(this.mHaveEglSurface) {
                                        if(GLSurfaceView2.this.mSizeChanged) {
                                            sizeChanged = true;
                                            w = this.mWidth;
                                            h = this.mHeight;
                                            wantRenderNotification = true;
                                            GLSurfaceView2.this.mSizeChanged = false;
                                        } else {
                                            this.mRequestRender = false;
                                        }

                                        GLSurfaceView2.sGLThreadManager.notifyAll();
                                        break;
                                    }
                                }

                                GLSurfaceView2.sGLThreadManager.wait();
                            }
                        }

                        if(event != null) {
                            event.run();
                            event = null;
                        } else {
                            if(createEglSurface) {
                                Log.w("GLThread", "egl createSurface");
                                gl = (GL10)this.mEglHelper.createSurface(GLSurfaceView2.this.getHolder());
                                if(gl == null) {
                                    var25 = false;
                                    break label347;
                                }

                                GLSurfaceView2.sGLThreadManager.checkGLDriver(gl);
                                createEglSurface = false;
                            }

                            if(createEglContext) {
                                this.mRenderer.onSurfaceCreated(gl, this.mEglHelper.mEglConfig);
                                createEglContext = false;
                            }

                            if(sizeChanged) {
                                this.mRenderer.onSurfaceChanged(gl, w, h);
                                sizeChanged = false;
                            }

                            this.mRenderer.onDrawFrame(gl);
                            if(this.mBufferSwap && !this.mEglHelper.swap()) {
                                Log.i("GLThread", "egl context lost tid=" + this.getId());
                                lostEglContext = true;
                            }

                            if(wantRenderNotification) {
                                doRenderNotification = true;
                            }
                        }
                    }
                } finally {
                    if(var25) {
                        synchronized(GLSurfaceView2.sGLThreadManager) {
                            this.stopEglSurfaceLocked();
                            this.stopEglContextLocked();
                        }
                    }
                }

                synchronized(GLSurfaceView2.sGLThreadManager) {
                    this.stopEglSurfaceLocked();
                    this.stopEglContextLocked();
                    return;
                }
            }

            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.stopEglSurfaceLocked();
                this.stopEglContextLocked();
            }
        }

        public void setRenderMode(int renderMode) {
            if(0 <= renderMode && renderMode <= 1) {
                synchronized(GLSurfaceView2.sGLThreadManager) {
                    this.mRenderMode = renderMode;
                    GLSurfaceView2.sGLThreadManager.notifyAll();
                }
            } else {
                throw new IllegalArgumentException("renderMode");
            }
        }

        public void setRenderer(GLSurfaceView2.Renderer renderer) {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mRenderer = renderer;
            }
        }

        public int getRenderMode() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                return this.mRenderMode;
            }
        }

        public void requestRender() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mRequestRender = true;
                GLSurfaceView2.sGLThreadManager.notifyAll();
            }
        }

        public void setBufferSwap(boolean isSwap) {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mBufferSwap = isSwap;
            }
        }

        public void surfaceCreated() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mHasSurface = true;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(this.mWaitingForSurface && !this.mExited) {
                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var4) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void surfaceDestroyed() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mHasSurface = false;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(!this.mWaitingForSurface && !this.mExited) {
                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var4) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void onPause() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mRequestPaused = true;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(!this.mExited && !this.mPaused) {
                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var4) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void onResume() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mRequestPaused = false;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(!this.mExited && this.mPaused && !this.mRenderComplete) {
                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var4) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void onWindowResize(int w, int h) {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mWidth = w;
                this.mHeight = h;
                GLSurfaceView2.this.mSizeChanged = true;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(!this.mExited && !this.mPaused && !this.mRenderComplete) {
                    Log.i("Main thread", "onWindowResize waiting for render complete.");

                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var6) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void requestExitAndWait() {
            synchronized(GLSurfaceView2.sGLThreadManager) {
                this.mShouldExit = true;
                GLSurfaceView2.sGLThreadManager.notifyAll();

                while(!this.mExited) {
                    try {
                        GLSurfaceView2.sGLThreadManager.wait();
                    } catch (InterruptedException var4) {
                        Thread.currentThread().interrupt();
                    }
                }

            }
        }

        public void queueEvent(Runnable r) {
            if(r == null) {
                throw new IllegalArgumentException("r must not be null");
            } else {
                synchronized(GLSurfaceView2.sGLThreadManager) {
                    this.mEventQueue.add(r);
                    GLSurfaceView2.sGLThreadManager.notifyAll();
                }
            }
        }
    }

    private class EglHelper {
        EGL10 mEgl;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;
        EGLConfig mEglConfig;
        EGLContext mEglContext;

        public EglHelper() {
        }

        public void start() {
            this.mEgl = (EGL10)EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            if(this.mEglDisplay == EGL10.EGL_NO_DISPLAY) {
                throw new RuntimeException("eglGetDisplay failed");
            } else {
                int[] version = new int[2];
                if(!this.mEgl.eglInitialize(this.mEglDisplay, version)) {
                    throw new RuntimeException("eglInitialize failed");
                } else {
                    this.mEglConfig = GLSurfaceView2.this.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
                    this.mEglContext = GLSurfaceView2.this.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
                    if(this.mEglContext == null || this.mEglContext == EGL10.EGL_NO_CONTEXT) {
                        this.mEglContext = null;
                        this.throwEglException("createContext");
                    }

                    this.mEglSurface = null;
                }
            }
        }

        public GL createSurface(SurfaceHolder holder) {
            if(this.mEgl == null) {
                throw new RuntimeException("egl not initialized");
            } else if(this.mEglDisplay == null) {
                throw new RuntimeException("eglDisplay not initialized");
            } else if(this.mEglConfig == null) {
                throw new RuntimeException("mEglConfig not initialized");
            } else {
                if(this.mEglSurface != null && this.mEglSurface != EGL10.EGL_NO_SURFACE) {
                    this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                    GLSurfaceView2.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                }

                this.mEglSurface = GLSurfaceView2.this.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, holder);
                if(this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE) {
                    int error = this.mEgl.eglGetError();
                    if(error == 12299) {
                        Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
                        return null;
                    }

                    this.throwEglException("createWindowSurface", error);
                }

                if(!this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext)) {
                    this.throwEglException("eglMakeCurrent");
                }

                GL gl = this.mEglContext.getGL();
                if(GLSurfaceView2.this.mGLWrapper != null) {
                    gl = GLSurfaceView2.this.mGLWrapper.wrap(gl);
                }

                if((GLSurfaceView2.this.mDebugFlags & 3) != 0) {
                    int configFlags = 0;
                    Writer log = null;
                    if((GLSurfaceView2.this.mDebugFlags & 1) != 0) {
                        configFlags |= 1;
                    }

                    if((GLSurfaceView2.this.mDebugFlags & 2) != 0) {
                        log = new GLSurfaceView2.LogWriter();
                    }

                    gl = GLDebugHelper.wrap(gl, configFlags, log);
                }

                return gl;
            }
        }

        public boolean swap() {
            if(!this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface)) {
                int error = this.mEgl.eglGetError();
                switch(error) {
                    case 12299:
                        Log.e("EglHelper", "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid=" + Thread.currentThread().getId());
                        break;
                    case 12302:
                        return false;
                    default:
                        this.throwEglException("eglSwapBuffers", error);
                }
            }

            return true;
        }

        public void destroySurface() {
            if(this.mEglSurface != null && this.mEglSurface != EGL10.EGL_NO_SURFACE) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                GLSurfaceView2.this.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }

        }

        public void finish() {
            if(this.mEglContext != null) {
                GLSurfaceView2.this.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }

            if(this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }

        }

        private void throwEglException(String function) {
            this.throwEglException(function, this.mEgl.eglGetError());
        }

        private void throwEglException(String function, int error) {
            String message = function + " failed";
            throw new RuntimeException(message);
        }
    }

    private class SimpleEGLConfigChooser extends GLSurfaceView2.ComponentSizeChooser {
        public SimpleEGLConfigChooser(boolean withDepthBuffer) {
            super(5, 6, 5, 0, withDepthBuffer?16:0, 0);
        }
    }

    private class ComponentSizeChooser extends GLSurfaceView2.BaseConfigChooser {
        private int[] mValue = new int[1];
        protected int mRedSize;
        protected int mGreenSize;
        protected int mBlueSize;
        protected int mAlphaSize;
        protected int mDepthSize;
        protected int mStencilSize;

        public ComponentSizeChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
            super(new int[]{12324, redSize, 12323, greenSize, 12322, blueSize, 12321, alphaSize, 12325, depthSize, 12326, stencilSize, 12344});
            this.mRedSize = redSize;
            this.mGreenSize = greenSize;
            this.mBlueSize = blueSize;
            this.mAlphaSize = alphaSize;
            this.mDepthSize = depthSize;
            this.mStencilSize = stencilSize;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            EGLConfig[] var4 = configs;
            int var5 = configs.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                EGLConfig config = var4[var6];
                int d = this.findConfigAttrib(egl, display, config, 12325, 0);
                int s = this.findConfigAttrib(egl, display, config, 12326, 0);
                if(d >= this.mDepthSize && s >= this.mStencilSize) {
                    int r = this.findConfigAttrib(egl, display, config, 12324, 0);
                    int g = this.findConfigAttrib(egl, display, config, 12323, 0);
                    int b = this.findConfigAttrib(egl, display, config, 12322, 0);
                    int a = this.findConfigAttrib(egl, display, config, 12321, 0);
                    if(r == this.mRedSize && g == this.mGreenSize && b == this.mBlueSize && a == this.mAlphaSize) {
                        return config;
                    }
                }
            }

            return null;
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue) {
            return egl.eglGetConfigAttrib(display, config, attribute, this.mValue)?this.mValue[0]:defaultValue;
        }
    }

    private abstract class BaseConfigChooser implements GLSurfaceView2.EGLConfigChooser {
        protected int[] mConfigSpec;

        public BaseConfigChooser(int[] configSpec) {
            this.mConfigSpec = this.filterConfigSpec(configSpec);
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            int[] num_config = new int[1];
            if(!egl.eglChooseConfig(display, this.mConfigSpec, (EGLConfig[])null, 0, num_config)) {
                throw new IllegalArgumentException("eglChooseConfig failed");
            } else {
                int numConfigs = num_config[0];
                if(numConfigs <= 0) {
                    throw new IllegalArgumentException("No configs match configSpec");
                } else {
                    EGLConfig[] configs = new EGLConfig[numConfigs];
                    if(!egl.eglChooseConfig(display, this.mConfigSpec, configs, numConfigs, num_config)) {
                        throw new IllegalArgumentException("eglChooseConfig#2 failed");
                    } else {
                        EGLConfig config = this.chooseConfig(egl, display, configs);
                        if(config == null) {
                            throw new IllegalArgumentException("No config chosen");
                        } else {
                            return config;
                        }
                    }
                }
            }
        }

        abstract EGLConfig chooseConfig(EGL10 var1, EGLDisplay var2, EGLConfig[] var3);

        private int[] filterConfigSpec(int[] configSpec) {
            if(GLSurfaceView2.this.mEGLContextClientVersion != 2) {
                return configSpec;
            } else {
                int len = configSpec.length;
                int[] newConfigSpec = new int[len + 2];
                System.arraycopy(configSpec, 0, newConfigSpec, 0, len - 1);
                newConfigSpec[len - 1] = 12352;
                newConfigSpec[len] = 4;
                newConfigSpec[len + 1] = 12344;
                return newConfigSpec;
            }
        }
    }

    public interface EGLConfigChooser {
        EGLConfig chooseConfig(EGL10 var1, EGLDisplay var2);
    }

    private static class DefaultWindowSurfaceFactory implements GLSurfaceView2.EGLWindowSurfaceFactory {
        private DefaultWindowSurfaceFactory() {
        }

        public EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config, Object nativeWindow) {
            return egl.eglCreateWindowSurface(display, config, nativeWindow, (int[])null);
        }

        public void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface) {
            egl.eglDestroySurface(display, surface);
        }
    }

    public interface EGLWindowSurfaceFactory {
        EGLSurface createWindowSurface(EGL10 var1, EGLDisplay var2, EGLConfig var3, Object var4);

        void destroySurface(EGL10 var1, EGLDisplay var2, EGLSurface var3);
    }

    private class DefaultContextFactory implements GLSurfaceView2.EGLContextFactory {
        private int EGL_CONTEXT_CLIENT_VERSION;

        private DefaultContextFactory() {
            this.EGL_CONTEXT_CLIENT_VERSION = 12440;
        }

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig config) {
            int[] attrib_list = new int[]{this.EGL_CONTEXT_CLIENT_VERSION, GLSurfaceView2.this.mEGLContextClientVersion, 12344};
            return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, GLSurfaceView2.this.mEGLContextClientVersion != 0?attrib_list:null);
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            if(!egl.eglDestroyContext(display, context)) {
                Log.e("DefaultContextFactory", "display:" + display + " context: " + context);
                throw new RuntimeException("eglDestroyContext failed");
            }
        }
    }

    public interface EGLContextFactory {
        EGLContext createContext(EGL10 var1, EGLDisplay var2, EGLConfig var3);

        void destroyContext(EGL10 var1, EGLDisplay var2, EGLContext var3);
    }

    public interface Renderer {
        void onSurfaceCreated(GL10 var1, EGLConfig var2);

        void onSurfaceChanged(GL10 var1, int var2, int var3);

        void onDrawFrame(GL10 var1);
    }

    public interface GLWrapper {
        GL wrap(GL var1);
    }
}
