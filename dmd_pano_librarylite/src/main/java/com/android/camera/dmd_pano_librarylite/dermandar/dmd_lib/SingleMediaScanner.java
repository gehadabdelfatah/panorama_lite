package com.android.camera.dmd_pano_librarylite.dermandar.dmd_lib;

/**
 * Created by Pc1 on 05/02/2018.
 */

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;

class SingleMediaScanner implements MediaScannerConnectionClient {
    private MediaScannerConnection mMs;
    private String mPath;

    public SingleMediaScanner(Context context, String path) {
        this.mPath = path;
        this.mMs = new MediaScannerConnection(context, this);
        this.mMs.connect();
    }

    public void onMediaScannerConnected() {
        this.mMs.scanFile(this.mPath, (String)null);
    }

    public void onScanCompleted(String path, Uri uri) {
        this.mMs.disconnect();
    }
}
